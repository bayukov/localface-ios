//
//  LFOtherProfileViewController.m
//  localface
//
//  Created by Кирилл Баюков on 22.03.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//

#import "LFOtherProfileViewController.h"
#import "LFProfileTopTableViewCell.h"
#import "LFRequester.h"
#import "UIImageView+AFNetworking.h"
#import "SVProgressHUD.h"
#import "UIColor+myColor.h"
#import "LFDefines.h"
#import <YandexMobileMetrica/YandexMobileMetrica.h>
#import "LFUserStatsTableViewCell.h"


@interface LFOtherProfileViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) NSDictionary* user;
@property (strong, nonatomic) NSArray* links;
@property (strong, nonatomic) LFProfileTopTableViewCell* topCell;
@property (nonatomic) BOOL blocked;
@property (strong, nonatomic) NSDictionary* socials;
@end

@implementation LFOtherProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.rowHeight=UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight=100;
    [self getUserInfo];
    UIBarButtonItem* back=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    back.tintColor=[UIColor myBlackColor];
    self.navigationItem.leftBarButtonItem=back;
    self.title=loc(@"PROFILE");
    self.tableView.separatorColor=[UIColor clearColor];
    UIBarButtonItem* contextMenu=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"contextmenu"] style:UIBarButtonItemStyleDone target:self action:@selector(contextMenuPressed)];
    contextMenu.tintColor=[UIColor myBlackColor];
    self.navigationItem.rightBarButtonItem=contextMenu;
    [self getStats];
    // Do any additional setup after loading the view.
}

-(void)getStats
{
    [[[LFRequester sharedInstanse] sessionManager] GET:@"statsForProfile2" parameters:@{@"user_id":@(self.userID)} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.socials=responseObject;
        [self.tableView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
    }];
}

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)getUserInfo
{
    [[[LFRequester sharedInstanse] sessionManager] GET:@"profile" parameters:@{@"id":@(self.userID), @"myID":[defaults valueForKey:@"myID"]} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [YMMYandexMetrica reportEvent:@"Просмотр профиля"
                           parameters:@{}
                            onFailure:^(NSError *error) {
                                NSLog(@"error: %@", [error localizedDescription]);
                            }];
        self.links=[[responseObject valueForKey:@"links"] copy];
        self.user=[[responseObject valueForKey:@"user"] copy];
        if ([[responseObject valueForKey:@"blocked"] intValue]==0)
            self.blocked=NO;
        else
            self.blocked=YES;
        [self.tableView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self back];
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0)
    {
        return 1;
    }
    else if (section==1)
    {
        return self.links.count;
    }
    else if (section==2)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0)
    {
        LFProfileTopTableViewCell* cell=[tableView dequeueReusableCellWithIdentifier:@"topCell"];
        [cell.image setImageWithURL:[NSURL URLWithString:[self.user valueForKey:@"photo"]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        cell.image.layer.cornerRadius=55;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.image.clipsToBounds=YES;
        cell.nameTextField.text=[self.user valueForKey:@"name"];
        cell.nameTextField.enabled=NO;
        if ([self.user valueForKey:@"vk"] && ![[self.user valueForKey:@"vk"] isKindOfClass:[NSNull class]])
        {
            cell.vkButton.alpha=1;
        }
        else
        {
            cell.vkButton.alpha=0.5;
        }
        if ([self.user valueForKey:@"fb"] && ![[self.user valueForKey:@"fb"] isKindOfClass:[NSNull class]])
        {
            cell.fbButton.alpha=1;
        }
        else
        {
            cell.fbButton.alpha=0.5;
        }
        if ([self.user valueForKey:@"insta"] && ![[self.user valueForKey:@"insta"] isKindOfClass:[NSNull class]])
        {
            cell.igButton.alpha=1;
        }
        else
        {
            cell.igButton.alpha=0.5;
        }
        if ([self.user valueForKey:@"twitter"] && ![[self.user valueForKey:@"twitter"] isKindOfClass:[NSNull class]])
        {
            cell.twitterButton.alpha=1;
        }
        else
        {
            cell.twitterButton.alpha=0.5;
        }
        self.topCell=cell;
        return cell;
    }
    else if (indexPath.section==1)
    {
        UITableViewCell* cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        UILabel* titleLabel=[cell viewWithTag:1];
        UILabel* linkLabel=[cell viewWithTag:2];
        titleLabel.text=[self.links[indexPath.row] valueForKey:@"title"];
        linkLabel.text=[self.links[indexPath.row] valueForKey:@"link"];
        return cell;
    }
    else if (indexPath.section==2)
    {
        LFUserStatsTableViewCell* cell=[tableView dequeueReusableCellWithIdentifier:@"statsCell"];
        cell.talkativityFlag=[[self.socials valueForKey:@"haveMessages"] boolValue];
        
        cell.sociabilityFlag=[[self.socials valueForKey:@"haveFriends"] boolValue];
        cell.contentGenerationFlag=[[self.socials valueForKey:@"havePosts"] boolValue];
        cell.responsivenessFlag=[[self.socials valueForKey:@"haveLikes"] boolValue];
        cell.musicInterestFlag=[[self.socials valueForKey:@"haveMusic"] boolValue]  ;
        cell.videoInterestFlag=[[self.socials valueForKey:@"haveMovies"] boolValue];
        cell.sociabilityProgressBar.progress=[[self.socials valueForKey:@"friends"] floatValue];
        cell.musicInterestProgressBar.progress=50.0/[[self.socials valueForKey:@"music"] floatValue];
        cell.videoInterestProgressBar.progress=[[self.socials valueForKey:@"movies"] floatValue];
        cell.contentGenerationProgressBar.progress=[[self.socials valueForKey:@"posts"] floatValue];
        cell.responsivenessProgressBar.progress=[[self.socials valueForKey:@"likes"] floatValue];
        cell.talkativityProgressBar.progress=[[self.socials valueForKey:@"messages"] floatValue];
        [cell reinit];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
    }
    else
    {
        return nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section==1)
    {
        NSString* link=[self.links[indexPath.row] valueForKey:@"link"];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[self.links[indexPath.row] valueForKey:@"link"]]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[self.links[indexPath.row] valueForKey:@"link"]]];
        }
        else if ([link characterAtIndex:0]=='+' && link.length==12)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",link]]];
        }
        else
        {
            [[UIPasteboard generalPasteboard] setString:[self.links[indexPath.row] valueForKey:@"link"]];
            [SVProgressHUD showSuccessWithStatus:loc(@"COPIED")];
        }
    }
}

- (IBAction)vkButtonPressed:(id)sender {
    if (self.topCell.vkButton.alpha==1)
    {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"vk://vk.com/%@",[self.user valueForKey:@"vk"]]]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"vk://vk.com/%@",[self.user valueForKey:@"vk"]]]];
        }
        else
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"htpp://vk.com/%@",[self.user valueForKey:@"vk"]]]];
        }
    }
}

- (IBAction)instaButtonPressed:(id)sender {
    if (self.topCell.igButton.alpha==1)
    {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"instagram://user?username=%@",[self.user valueForKey:@"insta"]]]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"instagram://user?username=%@",[self.user valueForKey:@"insta"]]]];
        }
        else
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.instagram.com/%@",[self.user valueForKey:@"insta"]]]];
        }
    }
}

- (IBAction)fbButtonPressed:(id)sender {
    if (self.topCell.fbButton.alpha==1)
    {
        //            else
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.facebook.com/app_scoped_user_id/%@",[self.user valueForKey:@"fb"]]]];
        }
    }
}

- (IBAction)twitterButtonPressed:(id)sender {
    if (self.topCell.twitterButton.alpha==1)
    {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"twitter:///user?screen_name=%@",[self.user valueForKey:@"twitter"]]]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://twitter.com/%@",[self.user valueForKey:@"twitter"]]]];
        }
        else
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://twitter.com/%@",[self.user valueForKey:@"twitter"]]]];
        }
    }
}

-(void)contextMenuPressed
{
    UIAlertController* alert=[UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* block=[UIAlertAction actionWithTitle:loc(@"BLOCK_USER") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [[[LFRequester sharedInstanse] sessionManager] POST:@"blockUser" parameters:@{@"id":[self.user valueForKey:@"id"], @"myID":[defaults valueForKey:@"myID"]} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            self.blocked=YES;
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"%@",error);
        }];
    }];
    UIAlertAction* unblock=[UIAlertAction actionWithTitle:loc(@"UNBLOCK_USER") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[[LFRequester sharedInstanse] sessionManager] POST:@"unblockUser" parameters:@{@"id":[self.user valueForKey:@"id"], @"myID":[defaults valueForKey:@"myID"]} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            self.blocked=NO;
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"%@",error);
        }];
    }];
    if (self.blocked)
    {
        [alert addAction:unblock];
    }
    else
    {
        [alert addAction:block];
    }
    [alert addAction:[UIAlertAction actionWithTitle:loc(@"CANCEL") style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}




/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
