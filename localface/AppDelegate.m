//
//  AppDelegate.m
//  localface
//
//  Created by Кирилл Баюков on 22.03.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//
@import GoogleMaps;
#import "AppDelegate.h"
#import "LFDefines.h"
#import "LFCoordSynchronizer.h"
#import "VKSdk.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>
#import <Crashlytics/Crashlytics.h>
#import "LFMaster.h"
#import "LFRequester.h"
#import <YandexMobileMetrica/YandexMobileMetrica.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    NSString* name;
    NSString* apiKey = @"fd36f9c4-ebbe-48f7-800e-4d5f26db5b7f";
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    if ([launchOptions valueForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"])
    {
        [self application:[UIApplication sharedApplication] didReceiveRemoteNotification:[launchOptions valueForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"]];
    }
    if (![defaults valueForKey:@"myID"])
    {
        name=@"login";
    }
    else
    {
        name=@"tabbar";
    }
    if (![defaults valueForKey:@"radius"])
    {
        [defaults setValue:@10000 forKey:@"radius"];
        [defaults synchronize];
    }
    
    if ([self class] == [AppDelegate class]) {
        [YMMYandexMetrica activateWithApiKey:apiKey];
        [YMMYandexMetrica setLoggingEnabled:YES];
    }
    
    UIViewController* controller=[self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:name];
    self.window.rootViewController=controller;
    [GMSServices provideAPIKey:@"AIzaSyDZGbZUo7xvnuJqbqISTzSc03uGQSzTi1w"];
    [[LFCoordSynchronizer sharedInstanse] startMonitoringPosition];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    [Fabric with:@[[Twitter class], [Crashlytics class]]];

    [FBSDKSettings setAppID:@"286532805092996"];
//    [FBSDKSettings setAppID:@"383212185428083"];
    [LFMaster refreshFBStats];
        // Override point for customization after application launch.
    return YES;
}

-(void)refreshStats
{
    [LFMaster refreshAllStats];
    [self performSelector:@selector(refreshStats) withObject:nil afterDelay:3600];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    //   NSLog(@"BOOOM");
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    [defaults setValue:token forKey:@"token"];
    [defaults synchronize];
    [[LFRequester sharedInstanse] sendToken];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    //   NSLog(@"FAIL");
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"token"];
    // [SVProgressHUD showErrorWithStatus:@"ERROR"];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    if ([defaults valueForKey:@"myID"])
    {
        [LFMaster requestShowingMyPosition];
        [[LFRequester sharedInstanse] sendToken];
    }
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    [VKSdk processOpenURL:url fromApplication:sourceApplication];
    return YES;
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [defaults setValue:[userInfo valueForKey:@"id"] forKey:@"idToOpen"];
    [defaults synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DID_OPEN_NOTIFICATION" object:nil];
}


@end
