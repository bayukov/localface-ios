//
//  LFMapViewController.m
//  localface
//
//  Created by Кирилл Баюков on 22.03.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//
@import GoogleMaps;
#import "LFMapViewController.h"
#import "LFDefines.h"
#import "LFRequester.h"
#import "UIImageView+AFNetworking.h"
#import "LFOtherProfileViewController.h"

@interface LFMapViewController ()<GMSMapViewDelegate>
@property (strong, nonatomic) GMSMapView* map;
@property (strong, nonatomic) NSMutableArray* markers;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;
@property (weak, nonatomic) IBOutlet UIButton *minusButton;

@end

@implementation LFMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.markers=[[NSMutableArray alloc] init];
    GMSCameraPosition *camera;
    if ([defaults valueForKey:@"lat"])
    {
        camera = [GMSCameraPosition cameraWithLatitude:[defaults floatForKey:@"lat"] longitude:[defaults floatForKey:@"lng"] zoom:12.0];
    }
    else
    {
        camera = [GMSCameraPosition cameraWithLatitude:55.75 longitude:37.617 zoom:11.0];
    }
    self.map = [GMSMapView mapWithFrame:self.view.frame camera:camera];
    UIEdgeInsets edge=UIEdgeInsetsMake(64, 0, 110, 0);
    self.map.settings.myLocationButton=YES;
    
    [self.map setPadding:edge];
    
    self.map.myLocationEnabled = YES;
    self.map.settings.rotateGestures=NO;
    
    self.map.delegate = self;
    [self.view insertSubview:self.map belowSubview:self.plusButton];
    [self getNearby];
//    [self reloadMarkers];
  //  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(coordsRecieved) name:COORDS_RECIEVED object:nil];
   // [self.navigationController.navigationBar setBarTintColor:[UIColor myGreenColor]] ;
    self.navigationController.navigationBar.translucent=NO;
    self.title=loc(@"MAP");
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self performSelector:@selector(loadProfile) withObject:nil afterDelay:1];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shallOpenProfile) name:@"DID_OPEN_NOTIFICATION" object:nil];
}

-(void)shallOpenProfile
{
    [self performSelector:@selector(loadProfile) withObject:nil afterDelay:1];
}

-(void)loadProfile
{
    if ([defaults valueForKey:@"idToOpen"])
    {
        [self performSegueWithIdentifier:@"segueToProfile" sender:[defaults valueForKey:@"idToOpen"]];
        [defaults removeObjectForKey:@"idToOpen"];
        [defaults synchronize];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getNearby
{
    CLLocationCoordinate2D topLeft=[self.map.projection coordinateForPoint:CGPointZero];
    CLLocationCoordinate2D bottomRight=[self.map.projection coordinateForPoint:CGPointMake(self.map.frame.size.width, self.map.frame.size.height)];
    
    
    NSDictionary *params = @{ @"lat1" : @(topLeft.latitude),@"lat2" : @(bottomRight.latitude),   @"long1" : @(topLeft.longitude),@"long2" : @(bottomRight.longitude), @"id":[defaults valueForKey:@"myID"] };
    [[[LFRequester sharedInstanse] sessionManager] GET:@"coords" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        for (GMSMarker* marker in self.markers) marker.map=nil;
        self.markers=[[NSMutableArray alloc] init];
        for (NSDictionary* user in responseObject)
        {
            GMSMarker* marker=[[GMSMarker alloc] init];
            marker.position=CLLocationCoordinate2DMake([[user valueForKey:@"lat"] floatValue], [[user valueForKey:@"lng"] floatValue]);
            marker.map=self.map;
            UIImageView* iconView=[[UIImageView alloc] initWithFrame:CGRectMake(30, 30, 60, 60)];
            iconView.layer.cornerRadius=30;
            iconView.layer.borderColor=[UIColor whiteColor].CGColor;
            iconView.layer.borderWidth=2;
            iconView.contentMode=UIViewContentModeScaleAspectFill;
            iconView.clipsToBounds=YES;
            [iconView setImageWithURL:[NSURL URLWithString:[user valueForKey:@"photo"]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
            marker.iconView=iconView;
            marker.userData=[user valueForKey:@"id"];
            [self.markers addObject:marker];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
    }];
    [self performSelector:@selector(getNearby) withObject:nil afterDelay:20];
}

-(void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(getNearby) object:nil];
    [self performSelector:@selector(getNearby) withObject:nil afterDelay:1];
}

-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    [self performSegueWithIdentifier:@"segueToProfile" sender:marker.userData];
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[LFOtherProfileViewController class]])
    {
        LFOtherProfileViewController* controller=segue.destinationViewController;
        controller.userID=[sender intValue];
    }
}

- (IBAction)plusButtonDown:(id)sender {
    [self zoomIn];
}

- (IBAction)plusButtonUp:(id)sender {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(zoomIn) object:nil];
    [self getNearby];
}

-(void)zoomIn
{
    GMSCameraUpdate* update=[GMSCameraUpdate zoomBy:0.03];
    [self.map moveCamera:update];
    [self performSelector:@selector(zoomIn) withObject:nil afterDelay:0.01];
}

- (IBAction)minusButtonDown:(id)sender {
    [self zoomOut];
}

- (IBAction)minusButtonUp:(id)sender {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(zoomOut) object:nil];
    [self getNearby];
}

-(void)zoomOut
{
    GMSCameraUpdate* update=[GMSCameraUpdate zoomBy:-0.03];
    [self.map moveCamera:update];
    [self performSelector:@selector(zoomOut) withObject:nil afterDelay:0.01];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
