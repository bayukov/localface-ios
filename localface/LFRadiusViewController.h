//
//  LFRadiusViewController.h
//  localface
//
//  Created by Кирилл Баюков on 27.03.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LFRadiusViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
