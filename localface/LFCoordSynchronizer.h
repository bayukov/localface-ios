//
//  LFCoordSynchronizer.h
//  localface
//
//  Created by Кирилл Баюков on 22.03.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LFCoordSynchronizer : NSObject
+(LFCoordSynchronizer*)sharedInstanse;
-(void)startMonitoringPosition;
@end
