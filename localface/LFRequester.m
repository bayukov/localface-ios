//
//  LFRequester.m
//  localface
//
//  Created by Кирилл Баюков on 22.03.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//

#import "LFRequester.h"
#import "LFDefines.h"

@implementation LFRequester
+(LFRequester *)sharedInstanse
{
    static LFRequester* requester;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        requester=[[LFRequester alloc] init];
        NSString* apiURL = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"API URL"];
        NSAssert(apiURL, @"Please specify API URL key in your Info.plist");
        requester.sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:apiURL]];
        requester.sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
        requester.sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
        requester.sessionManager.responseSerializer.acceptableContentTypes = [requester.sessionManager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        requester.sessionManager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        requester.sessionManager.securityPolicy.allowInvalidCertificates = YES;
        requester.sessionManager.securityPolicy.validatesDomainName = NO;
    });
    return requester;
}

-(void)setNewRadius:(NSNumber *)radius
{
    [[self sessionManager] POST:@"updateRadius" parameters:@{@"id":[defaults valueForKey:@"myID"], @"radius":radius} progress:nil success:nil failure:nil];
}

-(void)sendToken
{
    if ([defaults valueForKey:@"myID"] && [defaults valueForKey:@"token"])
    {
        [[self sessionManager] POST:@"sendToken" parameters:@{@"id":[defaults valueForKey:@"myID"], @"token":[defaults valueForKey:@"token"]} progress:nil success:nil failure:nil];
    }
}
@end
