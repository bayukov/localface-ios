//
//  LFSettingsViewController.m
//  localface
//
//  Created by Кирилл Баюков on 22.03.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//

#import "LFSettingsViewController.h"
#import "iRate.h"
#import "LFDefines.h"
#import <YandexMobileMetrica/YandexMobileMetrica.h>

@interface LFSettingsViewController ()
@property (weak, nonatomic) IBOutlet UIView *premiumView;
@property (weak, nonatomic) IBOutlet UIView *aboutView;
@property (weak, nonatomic) IBOutlet UIView *rateView;
@property (weak, nonatomic) IBOutlet UIView *radiusView;
@property (weak, nonatomic) IBOutlet UILabel *aboutLabel;
@property (weak, nonatomic) IBOutlet UILabel *rateLabel;
@property (weak, nonatomic) IBOutlet UILabel *radiusLabel;

@end

@implementation LFSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer* tap1=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(premiumViewPressed)];
    [self.premiumView addGestureRecognizer:tap1];
    self.premiumView.userInteractionEnabled=YES;
    
    UITapGestureRecognizer* tap2=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(aboutViewPressed)];
    [self.aboutView addGestureRecognizer:tap2];
    self.aboutView.userInteractionEnabled=YES;
    
    UITapGestureRecognizer* tap3=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rateViewPressed)];
    [self.rateView addGestureRecognizer:tap3];
    self.rateView.userInteractionEnabled=YES;
    
    UITapGestureRecognizer* tap4=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(radiusViewPressed)];
    [self.radiusView addGestureRecognizer:tap4];
    self.radiusView.userInteractionEnabled=YES;
    self.title=loc(@"SETTINGS");
    self.aboutLabel.text=loc(@"ABOUT");
    self.rateLabel.text=loc(@"RATE");
    self.radiusLabel.text=loc(@"DISPLAY_RADIUS");
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)premiumViewPressed
{
    
}

-(void)aboutViewPressed
{
    [self performSegueWithIdentifier:@"segueToAbout" sender:nil];
}

-(void)rateViewPressed
{
    [YMMYandexMetrica reportEvent:@"Оценка приложения"
                       parameters:@{}
                        onFailure:^(NSError *error) {
                            NSLog(@"error: %@", [error localizedDescription]);
                        }];
    [[iRate sharedInstance] promptForRating];
}

-(void)radiusViewPressed
{
    [self performSegueWithIdentifier:@"segueToRadius" sender:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
