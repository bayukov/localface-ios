//
//  LFUserStatsTableViewCell.h
//  localface
//
//  Created by Кирилл Баюков on 25.08.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LFUserStatsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *talkativity;
@property (weak, nonatomic) IBOutlet UILabel *sociability;
@property (weak, nonatomic) IBOutlet UILabel *contentGeneration;
@property (weak, nonatomic) IBOutlet UILabel *responsiveness;
@property (weak, nonatomic) IBOutlet UILabel *musicInterest;
@property (weak, nonatomic) IBOutlet UILabel *videoInterest;
@property (weak, nonatomic) IBOutlet UIProgressView *talkativityProgressBar;
@property (weak, nonatomic) IBOutlet UIProgressView *sociabilityProgressBar;
@property (weak, nonatomic) IBOutlet UIProgressView *contentGenerationProgressBar;
@property (weak, nonatomic) IBOutlet UIProgressView *responsivenessProgressBar;
@property (weak, nonatomic) IBOutlet UIProgressView *musicInterestProgressBar;
@property (weak, nonatomic) IBOutlet UIProgressView *videoInterestProgressBar;

@property(nonatomic) BOOL talkativityFlag;
@property(nonatomic) BOOL sociabilityFlag;
@property(nonatomic) BOOL contentGenerationFlag;
@property(nonatomic) BOOL responsivenessFlag;
@property(nonatomic) BOOL musicInterestFlag;
@property(nonatomic) BOOL videoInterestFlag;

-(void)reinit;
@end
