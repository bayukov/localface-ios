//
//  UIColor+myColor.m
//  localface
//
//  Created by Кирилл Баюков on 22.03.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//

#import "UIColor+myColor.h"

@implementation UIColor (myColor)

+(UIColor*)myGrayColor
{
    return [UIColor colorWithWhite:189.0/255 alpha:1];
}

+(UIColor*)myPinkColor
{
    return [UIColor colorWithRed:1 green:30.0/255 blue:130.0/255 alpha:1];
}

+(UIColor*)myBlackColor
{
    return [UIColor colorWithRed:35.0/255 green:31.0/255 blue:32.0/255 alpha:1];
}

@end
