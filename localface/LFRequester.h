//
//  LFRequester.h
//  localface
//
//  Created by Кирилл Баюков on 22.03.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface LFRequester : NSObject
+(LFRequester*)sharedInstanse;
-(void)setNewRadius:(NSNumber*)radius;
-(void)sendToken;
@property (strong, nonatomic) AFHTTPSessionManager* sessionManager;
@end
