//
//  UIImage+LM.h
//  largessme
//
//  Created by Александр Мурзанаев on 01.10.15.
//  Copyright © 2015 AppCraft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (LM)

- (UIColor *)averageColor;

- (UIImage *)shrinkedImage;

@end
