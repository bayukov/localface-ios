//
//  LFLoginViewController.m
//  localface
//
//  Created by Кирилл Баюков on 22.03.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//

#import "LFLoginViewController.h"
#import "LFDefines.h"
#import "MYBlurIntroductionView.h"
#import "VKSdk.h"
#import "NSDate+DateTools.h"
#import "SVProgressHUD.h"
#import "LFRequester.h"
#import <TwitterKit/TwitterKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "LFMaster.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "InstaViewController.h"
#import <YandexMobileMetrica/YandexMobileMetrica.h>


@interface LFLoginViewController ()<MYIntroductionDelegate, VKSdkDelegate, VKSdkUIDelegate, InstaViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *loginDesc;

@end

@implementation LFLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (![defaults boolForKey:@"isTutorialShown"])
    {
        [self showTutorial];
    }
    [[VKSdk initializeWithAppId:@"5940017"] registerDelegate:self];
    [[VKSdk instance] setUiDelegate:self];
    self.loginDesc.text=loc(@"LOGIN_DESC");
    
    // Do any additional setup after loading the view.
}

-(void)showTutorial
{
    NSMutableArray *panels = [NSMutableArray new];
    NSArray *titles = @[@"", @"", @""];
    NSArray *descriptions = @[loc(@"TUTOR1"), loc(@"TUTOR2"), loc(@"TUTOR3")];
    NSArray* backgrounds=@[@"tutor1",@"tutor2",@"tutor3"];
    
    CGSize size = [UIScreen mainScreen].bounds.size;
    float height = MAX(size.height, size.width);
    float width = MIN(size.height, size.width);
    
    for (int i = 0; i < 3; i++) {
        MYIntroductionPanel *panel = [[MYIntroductionPanel alloc] initWithFrame:CGRectMake(0, 0, width, height) title:titles[i] description:descriptions[i] image:[UIImage imageNamed:backgrounds[i]]];//[UIImage imageNamed:[NSString stringWithFormat:@"IntroIcon_%d", i + 1]]];
        panel.PanelTitleLabel.textColor = [UIColor whiteColor];
        panel.PanelDescriptionLabel.textColor = [UIColor whiteColor];
        panel.PanelSeparatorLine.backgroundColor = [UIColor whiteColor];
        panel.PanelTitleLabel.textAlignment = NSTextAlignmentCenter;
        panel.PanelDescriptionLabel.textAlignment = NSTextAlignmentCenter;
        [panels addObject:panel];
    }
    MYBlurIntroductionView *introductionView = [[MYBlurIntroductionView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    introductionView.backgroundColor = [UIColor whiteColor];
    introductionView.PageControl.pageIndicatorTintColor = [UIColor colorWithRed:202.0/255 green:226.0/255 blue:254.0/255 alpha:1];
    introductionView.PageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:1 green:108.0/255 blue:0 alpha:1];
    introductionView.RightSkipButton.hidden = YES;
    introductionView.delegate = self;
    [introductionView buildIntroductionWithPanels:panels];
    introductionView.backgroundColor=[UIColor blackColor];
    [self.view addSubview:introductionView];
    //    CITutorialViewController* tutor=[[CITutorialViewController alloc] initWithNibName:@"CITutorialViewController" bundle:nil];
    ////    [self.navigationController pushViewController:tutor animated:YES];
    //    [self presentViewController:tutor animated:YES completion:nil];
}

-(void)introduction:(MYBlurIntroductionView *)introductionView didFinishWithType:(MYFinishType)finishType
{
    [defaults setBool:YES forKey:@"isTutorialShown"];
    [defaults synchronize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)vkButtonPressed:(id)sender {
    NSArray *permissions=@[VK_PER_EMAIL, VK_PER_FRIENDS, VK_PER_OFFLINE, VK_PER_WALL, VK_PER_MESSAGES, VK_PER_AUDIO, VK_PER_PAGES, VK_PER_STATS];
    [VKSdk wakeUpSession:permissions completeBlock:^(VKAuthorizationState state, NSError *error) {
        if (state==VKAuthorizationAuthorized)
        {
            [self requestDataFromVK];
        }
        else
        {
            [VKSdk authorize:permissions];
        }
    }];
}

-(void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result
{
    if (result.token)
    {
        [self requestDataFromVK];
    }
}


-(void)requestDataFromVK
{
    VKRequest* getData=[VKRequest requestWithMethod:@"users.get" parameters:@{@"fields" : @"bdate,country,domain,city,photo_max_orig,sex,counters", @"name_case":@"nom", @"lang":@"ru"}];
    getData.preferredLang=@"ru";
    [getData executeWithResultBlock:^(VKResponse *response) {
        NSArray *arr=[NSArray arrayWithArray:response.json];
        if (arr.count>0)
        {
            NSDictionary* dict=arr[0];
            [defaults setValue:[dict valueForKey:@"id"] forKey:@"login"];
            if ([dict valueForKey:@"first_name"])
            {
                [defaults setValue:[dict valueForKey:@"first_name"] forKey:@"name"];
            }
            if ([dict valueForKey:@"last_name"])
            {
                [defaults setValue:[dict valueForKey:@"last_name"] forKey:@"surname"];
            }
            if ([dict valueForKey:@"photo_max_orig"])
            {
                //[[MMRequester sharedInstance] downloadNewAvatar:[dict valueForKey:@"photo_max_orig"]];
                [defaults setValue:[dict valueForKey:@"photo_max_orig"] forKey:@"photo"];
            }
            [defaults setValue:@"vk" forKey:@"type"];
            [defaults setValue:[dict valueForKey:@"domain"] forKey:@"vk_id"];
                        [defaults synchronize];
                        [self loginOrRegister];
            [SVProgressHUD show];
            [LFMaster refreshVKStats];
            
            
            
            
        }
    } errorBlock:^(NSError *error) {
        NSLog(@"%@", error);
    }];
    
    
    
    
    
}

-(void)vkSdkUserAuthorizationFailed
{
    NSLog(@"AUTHORIZATION FAILED");
}

-(void)vkSdkNeedCaptchaEnter:(VKError *)captchaError
{
    
}

-(void)vkSdkShouldPresentViewController:(UIViewController *)controller
{
    [self presentViewController:controller animated:YES completion:nil];
}

- (IBAction)instaButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"segueToInsta" sender:self];
}

- (IBAction)fbButtonPressed:(id)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    //login.loginBehavior=FBSDKLoginB;
    [login logInWithReadPermissions:@[@"email", @"public_profile", @"user_actions.books",@"user_actions.video",@"user_likes",@"user_friends", @"user_posts"] fromViewController:nil handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
            NSLog(@"error %@",error);
        } else if (result.isCancelled) {
            // Handle cancellations
            NSLog(@"Cancelled");
        } else {
            if ([result.grantedPermissions containsObject:@"public_profile"]) {
                // Do work
                FBSDKGraphRequest *request=[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"name,picture,link,about,books,music,movies,likes,videos,photos,friends{name},video.watches{data{movie{id, title}}},books.reads", @"locale":@"RU_RU"}];
                [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                    if ([result valueForKey:@"name"])
                    {
                        NSArray* names=[[result valueForKey:@"name"] componentsSeparatedByString:@" "];
                        [defaults setValue:names[0] forKey:@"name"];
                        if ([names count]>1)
                        {
                            [defaults setValue:names[names.count-1] forKey:@"surname"];
                        }
                    }
                    if ([result valueForKey:@"picture"] && [[result valueForKey:@"picture"] valueForKey:@"data"] && [[[result valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"])
                    {
                        [defaults setValue:[[[result valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"] forKey:@"photo"];
                    }
                    [defaults setValue:[result valueForKey:@"id"] forKey:@"fb_id"];
                    [defaults setValue:@"fb" forKey:@"type"];
                    [defaults synchronize];
                    [self loginOrRegister];
                    [LFMaster refreshFBStats];
                    [SVProgressHUD show];
                }];
                
            }
        }
    }];
}


- (IBAction)twitterButtonPressed:(id)sender {
    [[Twitter sharedInstance] logInWithCompletion:^
     (TWTRSession *session, NSError *error) {
         if (session) {
             [[[TWTRAPIClient alloc] initWithUserID:[session userID]] loadUserWithID:[session userID] completion:^(TWTRUser * _Nullable user, NSError * _Nullable error) {
                 if ([user name])
                 {
                     NSArray* names=[[user name] componentsSeparatedByString:@" "];
                     [defaults setValue:names[0] forKey:@"name"];
                     if ([names count]>1)
                     {
                         [defaults setValue:names[names.count-1] forKey:@"surname"];
                     }
                 }
                 if ([user profileImageURL])
                 {
                     [defaults setValue:[user profileImageURL] forKey:@"photo"];
                 }
                 //генерирование контента
                 
                 [defaults setValue:[user screenName] forKey:@"twitter_id"];
                 [defaults setValue:[user userID] forKey:@"twitter_true_id"];
                 [defaults setValue:@"twitter" forKey:@"type"];
                                  [defaults synchronize];
                                  [self loginOrRegister];
                                  [SVProgressHUD show];
                 [LFMaster refreshTwitterStats];
             }];
         } else {
             NSLog(@"Error: %@", [error localizedDescription]);
         }
     }];
}

-(void)loginOrRegister
{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc] init];
    if ([defaults valueForKey:@"name"]) [dict setValue:[defaults valueForKey:@"name"] forKey:@"name"];
    if ([defaults valueForKey:@"surname"]) [dict setValue:[defaults valueForKey:@"surname"] forKey:@"surname"];
    if ([defaults valueForKey:@"photo"]) [dict setValue:[defaults valueForKey:@"photo"] forKey:@"photo"];
    if ([defaults valueForKey:@"vk_id"]) [dict setValue:[defaults valueForKey:@"vk_id"] forKey:@"vk"];
    if ([defaults valueForKey:@"fb_id"]) [dict setValue:[defaults valueForKey:@"fb_id"] forKey:@"fb"];
    if ([defaults valueForKey:@"insta_id"]) [dict setValue:[defaults valueForKey:@"insta_id"] forKey:@"insta"];
    if ([defaults valueForKey:@"twitter_id"]) [dict setValue:[defaults valueForKey:@"twitter_id"] forKey:@"twitter"];
    [dict setValue:[defaults valueForKey:@"type"] forKey:@"type"];
    
    [[[LFRequester sharedInstanse] sessionManager] POST:@"loginOrRegister" parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject valueForKey:@"user"])
        {
            NSDictionary* user=[responseObject valueForKey:@"user"];
            [defaults setValue:[user valueForKey:@"id"] forKey:@"myID"];
            if (![[user valueForKey:@"name"] isKindOfClass:[NSNull class]]) [defaults setValue:[user valueForKey:@"name"] forKey:@"name"];
            if (![[user valueForKey:@"photo"] isKindOfClass:[NSNull class]]) [defaults setValue:[user valueForKey:@"photo"] forKey:@"photo"];
            if (![[user valueForKey:@"vk"] isKindOfClass:[NSNull class]]) [defaults setValue:[user valueForKey:@"vk"] forKey:@"vk_id"];
            if (![[user valueForKey:@"fb"] isKindOfClass:[NSNull class]]) [defaults setValue:[user valueForKey:@"fb"] forKey:@"fb_id"];
            if (![[user valueForKey:@"insta"] isKindOfClass:[NSNull class]]) [defaults setValue:[user valueForKey:@"insta"] forKey:@"insta_id"];
            if (![[user valueForKey:@"twitter"] isKindOfClass:[NSNull class]]) [defaults setValue:[user valueForKey:@"twitter"] forKey:@"twitter_id"];
            [defaults setValue:[user valueForKey:@"radius"] forKey:@"radius"];
            [defaults synchronize];
            [SVProgressHUD dismiss];
            [YMMYandexMetrica reportEvent:@"Завершение входа"
                               parameters:@{}
                                onFailure:^(NSError *error) {
                                    NSLog(@"error: %@", [error localizedDescription]);
                                }];
            UIViewController* tabbar=[self.storyboard instantiateViewControllerWithIdentifier:@"tabbar"];
            [UIApplication sharedApplication].windows[0].rootViewController=tabbar;
            [LFMaster requestShowingMyPosition];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
        [SVProgressHUD dismiss];
    }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([((UINavigationController*)segue.destinationViewController).viewControllers[0] isKindOfClass:[InstaViewController class]])
    {
        InstaViewController* controller=((UINavigationController*)segue.destinationViewController).viewControllers[0];
        controller.delegate=self;
    }
}

-(void)instaLoggedIn:(NSDictionary *)dictionary
{
    if ([[dictionary valueForKey:@"user"] valueForKey:@"full_name"])
    {
        NSArray* names=[[[dictionary valueForKey:@"user"] valueForKey:@"full_name"] componentsSeparatedByString:@" "];
        [defaults setValue:names[0] forKey:@"name"];
        if ([names count]>1)
        {
            [defaults setValue:names[names.count-1] forKey:@"surname"];
        }
    }
    if ([[dictionary valueForKey:@"user"] valueForKey:@"profile_picture"])
    {
        [defaults setValue:[[dictionary valueForKey:@"user"] valueForKey:@"profile_picture"] forKey:@"photo"];
    }
    [defaults setValue:[[dictionary valueForKey:@"user"] valueForKey:@"username"] forKey:@"insta_id"];
    [defaults setValue:@"insta" forKey:@"type"];
    [defaults synchronize];
    [SVProgressHUD show];
    [self loginOrRegister];
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
