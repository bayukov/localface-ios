//
//  LFAboutViewController.m
//  localface
//
//  Created by Кирилл Баюков on 03.04.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//

#import "LFAboutViewController.h"
#import "UIColor+myColor.h"
#import "LFDefines.h"

@interface LFAboutViewController ()
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation LFAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem* back=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStyleDone target:self action:@selector(back)];
    back.tintColor=[UIColor myBlackColor];
    self.navigationItem.leftBarButtonItem=back;
    self.infoLabel.text=loc(@"ABOUT_TEXT");
    self.title=loc(@"ABOUT");
    self.scrollView.contentInset=UIEdgeInsetsMake(0, 0, -48, 0);
    self.scrollView.scrollIndicatorInsets=UIEdgeInsetsMake(-64, 0, -50, 0);
}


//\r\n______\r\nПриложение является бесплатным для загрузки и использования. Мы также предлагаем дополнительную платную подписку на премиум-аккаунт. Премиум-аккаунт включает в себя неограниченное количество переходов на профили пользователей в различных соцсетях.\r\nУсловия подписки: \r\n- Стоимость подписки на премиум-аккаунт составляет 299 руб. за 1 месяц, 699 руб. за 3 месяца, 999 руб. за 6 месяцев.\r\n    - Соответствующая сумма будет снята с Вашего iTunes-аккаунта при подтверждении покупки.\r\n- Подписка возобновляется автоматически, пока Вы ее не отмените. Это можно сделать не менее чем за 24 часа до истечения срока действия текущей подписки.\r\n- Оплата за обновление будет снята с вашего счета в течение 24 часов до окончания текущего периода подписки.\r\n- После приобретения подписки у вас появится возможность управлять ею, а также выключить автоматическое обновление в настройках аккаунта.\r\n- Вы не сможете отменить текущую подписку в течение того времени, пока она активна.\r\n- Неиспользованная часть бесплатного ознакомительного периода подписки будет аннулирована, если вы приобретете подписку в течение этого периода.

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
