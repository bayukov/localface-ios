//
//  LFCoordSynchronizer.m
//  localface
//
//  Created by Кирилл Баюков on 22.03.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//

#import "LFCoordSynchronizer.h"
#import <CoreLocation/CoreLocation.h>
#import "LFDefines.h"
#import "LFRequester.h"
@interface LFCoordSynchronizer()<CLLocationManagerDelegate>
@property(strong, nonatomic)CLLocationManager* manager;
@end

@implementation LFCoordSynchronizer


+(LFCoordSynchronizer*)sharedInstanse
{
    static LFCoordSynchronizer* req;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        req=[[LFCoordSynchronizer alloc] init];
    });
    return req;
}


-(void)startMonitoringPosition
{
    self.manager=[[CLLocationManager alloc] init];
    self.manager.delegate=self;
    [self.manager requestAlwaysAuthorization];
    [self.manager startMonitoringSignificantLocationChanges];
    self.manager.allowsBackgroundLocationUpdates=YES;
    self.manager.distanceFilter=500;
    [self performSelector:@selector(getQuickLocationUpdate) withObject:nil afterDelay:20];
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    [defaults setDouble:locations[0].coordinate.latitude forKey:@"lat"];
    [defaults setDouble:locations[0].coordinate.longitude forKey:@"lng"];
    [defaults synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:COORDS_UPDATED object:nil];
    if ([defaults valueForKey:@"myID"])
    {
        [[[LFRequester sharedInstanse] sessionManager] POST:@"updateCoords" parameters:@{@"id":[defaults valueForKey:@"myID"], @"lat":@(locations[0].coordinate.latitude), @"lng":@(locations[0].coordinate.longitude)} progress:nil success:nil failure:nil];
    }
}


-(void)getQuickLocationUpdate {
    // Request location authorization
    [self.manager requestWhenInUseAuthorization];
    
    // Request a location update
    [self.manager requestLocation];
    [self performSelector:@selector(getQuickLocationUpdate) withObject:nil afterDelay:20];
    // Note: requestLocation may timeout and produce an error if authorization has not yet been granted by the user
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    
}

@end
