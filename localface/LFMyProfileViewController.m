//
//  LFMyProfileViewController.m
//  localface
//
//  Created by Кирилл Баюков on 22.03.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//
#import "LFProfileTopTableViewCell.h"
#import "LFMyProfileViewController.h"
#import "LFDefines.h"
#import "SVProgressHUD.h"
#import "UIImageView+AFNetworking.h"
#import "LFRequester.h"
#import "VKSdk.h"
#import <TwitterKit/TwitterKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "InstaViewController.h"
#import "UIImage+LM.h"
#import "UIColor+myColor.h"
#import <YandexMobileMetrica/YandexMobileMetrica.h>
#import "LFUserStatsTableViewCell.h"
#import "LFMaster.h"

@interface LFMyProfileViewController ()<UITableViewDelegate, UITableViewDataSource,VKSdkDelegate, VKSdkUIDelegate, InstaViewControllerDelegate, UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) LFProfileTopTableViewCell* topCell;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addContactViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *addContactView;
@property (weak, nonatomic) IBOutlet UIButton *addContactButton;
@property (nonatomic) BOOL isEditing;
@property (strong, nonatomic) NSArray* links;
@property BOOL wasPhotoChanged;
@property (strong, nonatomic) NSDictionary* socials;
@end

@implementation LFMyProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.separatorColor=[UIColor clearColor];
    self.tableView.rowHeight=UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight=50;
    self.title=loc(@"MY_PROFILE");
    [self getContacts];
    [self endEdit];
    [[VKSdk initializeWithAppId:@"5940017"] registerDelegate:self];
    [[VKSdk instance] setUiDelegate:self];
    [self.addContactButton setTitle:loc(@"ADD_CONTACT") forState:UIControlStateNormal];
    [self getStats];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getStats) name:@"STATS_REFRESHED" object:nil];
    
    // Do any additional setup after loading the view.
}

-(void)getStats
{
    [[[LFRequester sharedInstanse] sessionManager] GET:@"statsForProfile2" parameters:@{@"user_id":[defaults valueForKey:@"myID"]} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.socials=responseObject;
        [self.tableView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
    }];
}


-(void)getContacts
{
    [[[LFRequester sharedInstanse] sessionManager] GET:@"links" parameters:@{@"user_id":[defaults valueForKey:@"myID"]} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.links=[responseObject copy];
        [self.tableView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
    }];
}

-(void)beginEdit
{
    UIBarButtonItem* edit=[[UIBarButtonItem alloc] initWithTitle:loc(@"SAVE") style:UIBarButtonItemStyleDone target:self action:@selector(save)];
    edit.tintColor=[UIColor colorWithRed:2.0/255 green:211.0/255 blue:0 alpha:1];
    self.navigationItem.rightBarButtonItem=edit;
    self.isEditing=YES;
    self.tableView.backgroundColor=[UIColor colorWithWhite:252.0/255 alpha:1];
    [self.tableView reloadData];
    self.addContactViewHeightConstraint.constant=50;
    [self.view layoutIfNeeded];
    self.addContactView.hidden=NO;
    
    UIBarButtonItem* cancel=[[UIBarButtonItem alloc] initWithTitle:loc(@"CANCEL") style:UIBarButtonItemStylePlain target:self action:@selector(endEdit)];
    cancel.tintColor=[UIColor colorWithRed:222.0/255 green:50.0/255 blue:0 alpha:1];
    self.navigationItem.leftBarButtonItem=cancel;
}

-(void)save
{
    if (![self.topCell.nameTextField.text isEqualToString:[defaults valueForKey:@"name"]] || self.wasPhotoChanged)
    {
        [SVProgressHUD show];
        [[[LFRequester sharedInstanse] sessionManager] POST:@"updateProfile" parameters:@{@"id":[defaults valueForKey:@"myID"], @"name":self.topCell.nameTextField.text} constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            if (self.wasPhotoChanged)
            {
                [formData appendPartWithFileData:UIImageJPEGRepresentation(self.topCell.image.image, 0.9) name:@"image" fileName:@"image" mimeType:@"image/jpeg"];
            }
        } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [defaults setValue:[responseObject valueForKey:@"name"] forKey:@"name"];
            [defaults setValue:[responseObject valueForKey:@"photo"] forKey:@"photo"];
            [defaults synchronize];
            self.wasPhotoChanged=NO;
            [YMMYandexMetrica reportEvent:@"Обновление профиля"
                               parameters:@{}
                                onFailure:^(NSError *error) {
                                    NSLog(@"error: %@", [error localizedDescription]);
                                }];
            [self endEdit];
            [SVProgressHUD showSuccessWithStatus:loc(@"PROFILE_SAVED")];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"%@",error);
            [SVProgressHUD showErrorWithStatus:loc(@"PROFILE_ERROR")];
        }];
    }
    else
    {
        [self endEdit];
    }
}

-(void)changeAvatarButtonPressed
{
    UIAlertController* alert=[UIAlertController alertControllerWithTitle:loc(@"UPDATE_IMAGE") message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:loc(@"TAKE_PICTURE") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self takePicture];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:loc(@"PICK_FROM_GALLERY") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self pickFromGallery];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:loc(@"CANCEL") style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}



-(void)takePicture
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

-(void)pickFromGallery
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        UIImage *image = info[UIImagePickerControllerEditedImage] ?: info[UIImagePickerControllerOriginalImage];
        image=[image shrinkedImage];
        self.topCell.image.image=image;
        self.wasPhotoChanged=YES;
    }];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)endEdit
{
    UIBarButtonItem* edit=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"edit"] style:UIBarButtonItemStyleDone target:self action:@selector(beginEdit)];
    edit.tintColor=[UIColor myBlackColor];
    self.navigationItem.rightBarButtonItem=edit;
    self.isEditing=NO;
    self.tableView.backgroundColor=[UIColor colorWithWhite:1 alpha:1];
    [self.tableView reloadData];
    self.addContactViewHeightConstraint.constant=0;
    [self.view layoutIfNeeded];
    self.addContactView.hidden=YES;
    
    self.navigationItem.leftBarButtonItem=nil;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0)
    {
        return 1;
    }
    else if (section==1)
    {
        return self.links.count;
    }
    else if (section==2)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0)
    {
        LFProfileTopTableViewCell* cell=[tableView dequeueReusableCellWithIdentifier:@"topCell"];
        self.topCell=cell;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell.image setImageWithURL:[NSURL URLWithString:[defaults valueForKey:@"photo"]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        cell.image.layer.cornerRadius=55;
        cell.image.clipsToBounds=YES;
        cell.nameTextField.text=[defaults valueForKey:@"name"];
        if ([defaults valueForKey:@"vk_id"])
        {
            cell.vkButton.alpha=1;
        }
        else
        {
            cell.vkButton.alpha=0.5;
        }
        
        if ([defaults valueForKey:@"fb_id"])
        {
            cell.fbButton.alpha=1;
        }
        else
        {
            cell.fbButton.alpha=0.5;
        }
        
        if ([defaults valueForKey:@"insta_id"])
        {
            cell.igButton.alpha=1;
        }
        else
        {
            cell.igButton.alpha=0.5;
        }
        
        if ([defaults valueForKey:@"twitter_id"])
        {
            cell.twitterButton.alpha=1;
        }
        else
        {
            cell.twitterButton.alpha=0.5;
        }
        if (self.isEditing)
        {
            cell.nameTextField.enabled=YES;
            cell.backgroundColor=[UIColor colorWithWhite:252.0/255 alpha:1];
            UITapGestureRecognizer* tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeAvatarButtonPressed)];
            [cell.image addGestureRecognizer:tap];
            cell.image.userInteractionEnabled=YES;
        }
        else
        {
            for (UIGestureRecognizer* gest in cell.image.gestureRecognizers)
            {
                [cell.image removeGestureRecognizer:gest];
            }
            cell.nameTextField.enabled=NO;
            cell.backgroundColor=[UIColor clearColor];
        }
        return cell;
    }
    else if (indexPath.section==1)
    {
        UITableViewCell* cell;
        if (self.isEditing)
        {
            cell=[tableView dequeueReusableCellWithIdentifier:@"cellEdit"];
            cell.backgroundColor=[UIColor colorWithWhite:252.0/255 alpha:1];
        }
        else
        {
            cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
            cell.backgroundColor=[UIColor whiteColor];
        }
        UILabel* titleLabel=[cell viewWithTag:1];
        UILabel* linkLabel=[cell viewWithTag:2];
        titleLabel.text=[self.links[indexPath.row] valueForKey:@"title"];
        linkLabel.text=[self.links[indexPath.row] valueForKey:@"link"];
        return cell;
    }
    else if (indexPath.section==2)
    {
        LFUserStatsTableViewCell* cell=[tableView dequeueReusableCellWithIdentifier:@"statsCell"];
        cell.talkativityFlag=[[self.socials valueForKey:@"haveMessages"] boolValue];
        
        cell.sociabilityFlag=[[self.socials valueForKey:@"haveFriends"] boolValue];
        cell.contentGenerationFlag=[[self.socials valueForKey:@"havePosts"] boolValue];
        cell.responsivenessFlag=[[self.socials valueForKey:@"haveLikes"] boolValue];
        cell.musicInterestFlag=[[self.socials valueForKey:@"haveMusic"] boolValue]  ;
        cell.videoInterestFlag=[[self.socials valueForKey:@"haveMovies"] boolValue];
        cell.sociabilityProgressBar.progress=[[self.socials valueForKey:@"friends"] floatValue];
        cell.musicInterestProgressBar.progress=50.0/[[self.socials valueForKey:@"music"] floatValue];
        cell.videoInterestProgressBar.progress=[[self.socials valueForKey:@"movies"] floatValue];
        cell.contentGenerationProgressBar.progress=[[self.socials valueForKey:@"posts"] floatValue];
        cell.responsivenessProgressBar.progress=[[self.socials valueForKey:@"likes"] floatValue];
        cell.talkativityProgressBar.progress=[[self.socials valueForKey:@"messages"] floatValue];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        [cell reinit];
        return cell;
    }
    else
    {
        return nil;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (indexPath.section==1)
    {
        NSString* link=[self.links[indexPath.row] valueForKey:@"link"];
        if (!self.isEditing)
        {
            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[self.links[indexPath.row] valueForKey:@"link"]]])
            {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[self.links[indexPath.row] valueForKey:@"link"]]];
            }
            else if ([link characterAtIndex:0]=='+' && link.length==12)
            {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",link]]];
            }
            else
            {
                [[UIPasteboard generalPasteboard] setString:[self.links[indexPath.row] valueForKey:@"link"]];
                [SVProgressHUD showSuccessWithStatus:loc(@"COPIED")];
            }
        }
        else
        {
            UIAlertController* alert=[UIAlertController alertControllerWithTitle:[self.links[indexPath.row] valueForKey:@"title"] message:nil preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:loc(@"DELETE") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                [[[LFRequester sharedInstanse] sessionManager] POST:@"removeLink" parameters:@{@"id":[self.links[indexPath.row] valueForKey:@"id"], @"user_id":[defaults valueForKey:@"myID"]} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    self.links=responseObject;
                    [self.tableView reloadData];
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    NSLog(@"%@",error);
                }];
            }]];
            [alert addAction:[UIAlertAction actionWithTitle:loc(@"EDIT") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                UIAlertController* alert2=[UIAlertController alertControllerWithTitle:loc(@"EDIT_CONTACT") message:nil preferredStyle:UIAlertControllerStyleAlert];
                [alert2 addAction:[UIAlertAction actionWithTitle:@"ОК" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [[[LFRequester sharedInstanse] sessionManager] POST:@"editLink" parameters:@{@"id":[self.links[indexPath.row] valueForKey:@"id"], @"user_id":[defaults valueForKey:@"myID"],@"title":alert2.textFields[0].text, @"link":alert2.textFields[1].text} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                        self.links=responseObject;
                        [self.tableView reloadData];
                    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                        NSLog(@"%@",error);
                    }];
                }]];
                [alert2 addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
                    textField.placeholder=loc(@"LINK_TITLE");
                    textField.text=[self.links[indexPath.row] valueForKey:@"title"];
                    textField.autocapitalizationType=UITextAutocapitalizationTypeSentences;
                }];
                [alert2 addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
                    textField.placeholder=loc(@"LINK_URL");
                    textField.text=[self.links[indexPath.row] valueForKey:@"link"];
                }];;
                [alert2 addAction:[UIAlertAction actionWithTitle:loc(@"CANCEL") style:UIAlertActionStyleCancel handler:nil]];
                [self presentViewController:alert2 animated:YES completion:nil];
            }]];
            [alert addAction:[UIAlertAction actionWithTitle:loc(@"CANCEL") style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)vkButtonPressed:(id)sender {
    if (self.isEditing)
    {
        if (self.topCell.vkButton.alpha==1)
        {
            
            UIAlertController* controller=[UIAlertController alertControllerWithTitle:loc(@"LOGOUT?") message:loc(@"VK") preferredStyle:UIAlertControllerStyleAlert];
            [controller addAction:[UIAlertAction actionWithTitle:@"ОK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                [self removeSocialWithType:@"vk"];
                [VKSdk forceLogout];
            }]];
            [controller addAction:[UIAlertAction actionWithTitle:loc(@"CANCEL") style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:controller animated:YES completion:nil];
        }
        else
        {
            NSArray *permissions=@[VK_PER_EMAIL, VK_PER_FRIENDS, VK_PER_OFFLINE, VK_PER_WALL, VK_PER_MESSAGES, VK_PER_AUDIO, VK_PER_PAGES, VK_PER_STATS];
            [VKSdk authorize:permissions];
//            [VKSdk wakeUpSession:permissions completeBlock:^(VKAuthorizationState state, NSError *error) {
//                if (state==VKAuthorizationAuthorized)
//                {
//                    [self requestDataFromVK];
//                }
//                else
//                {
//                    [VKSdk authorize:permissions];
//                }
//            }];
        }
    }
    else
    {
        if (self.topCell.vkButton.alpha==1)
        {
            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"vk://vk.com/%@",[defaults valueForKey:@"vk_id"]]]])
            {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"vk://vk.com/%@",[defaults valueForKey:@"vk_id"]]]];
            }
            else
            {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"htpp://vk.com/%@",[defaults valueForKey:@"vk_id"]]]];
            }
        }
    }
}

-(void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result
{
    if (result.token)
    {
        [self requestDataFromVK];
    }
}


-(void)requestDataFromVK
{
    VKRequest* getData=[VKRequest requestWithMethod:@"users.get" parameters:@{@"fields" : @"bdate,country,city,photo_max_orig,sex,domain", @"name_case":@"nom", @"lang":@"ru"}];
    getData.preferredLang=@"ru";
    [getData executeWithResultBlock:^(VKResponse *response) {
        NSArray *arr=[NSArray arrayWithArray:response.json];
        if (arr.count>0)
        {
            NSDictionary* dict=arr[0];
            [defaults setValue:[dict valueForKey:@"domain"] forKey:@"vk_id"];
            [defaults synchronize];
            [LFMaster refreshVKStats];
            [self addSocialWithType:@"vk" andID:[dict valueForKey:@"domain"]];
        }
//        [VKSdk forceLogout];
    } errorBlock:^(NSError *error) {
        NSLog(@"%@", error);
    }];
}

-(void)vkSdkUserAuthorizationFailed
{
    NSLog(@"AUTHORIZATION FAILED");
}

-(void)vkSdkNeedCaptchaEnter:(VKError *)captchaError
{
    
}

-(void)vkSdkShouldPresentViewController:(UIViewController *)controller
{
    [self presentViewController:controller animated:YES completion:nil];
}

- (IBAction)instaButtonPressed:(id)sender {
    if (self.isEditing)
    {
        if (self.topCell.igButton.alpha==1)
        {
            UIAlertController* controller=[UIAlertController alertControllerWithTitle:loc(@"LOGOUT?") message:@"Instagram" preferredStyle:UIAlertControllerStyleAlert];
            [controller addAction:[UIAlertAction actionWithTitle:@"ОK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                [self removeSocialWithType:@"insta"];
            }]];
            [controller addAction:[UIAlertAction actionWithTitle:loc(@"CANCEL") style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:controller animated:YES completion:nil];
        }
        else
        {
            [self performSegueWithIdentifier:@"segueToInsta" sender:self];
        }
    }
    else
    {
        if (self.topCell.igButton.alpha==1)
        {
            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"instagram://user?username=%@",[defaults valueForKey:@"insta_id"]]]])
            {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"instagram://user?username=%@",[defaults valueForKey:@"insta_id"]]]];
            }
            else
            {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.instagram.com/%@",[defaults valueForKey:@"insta_id"]]]];
            }
        }
    }
}

- (IBAction)fbButtonPressed:(id)sender {
    if (self.isEditing)
    {
        if (self.topCell.fbButton.alpha==1)
        {
            UIAlertController* controller=[UIAlertController alertControllerWithTitle:loc(@"LOGOUT?") message:@"Facebook" preferredStyle:UIAlertControllerStyleAlert];
            [controller addAction:[UIAlertAction actionWithTitle:@"ОK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                [self removeSocialWithType:@"fb"];
            }]];
            [controller addAction:[UIAlertAction actionWithTitle:loc(@"CANCEL") style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:controller animated:YES completion:nil];
        }
        else
        {
            FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
            //login.loginBehavior=FBSDKLoginB;
            [login logInWithReadPermissions:@[@"email", @"public_profile", @"user_actions.books",@"user_actions.video",@"user_likes",@"user_friends", @"user_posts"] fromViewController:nil handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                if (error) {
                    // Process error
                    NSLog(@"error %@",error);
                } else if (result.isCancelled) {
                    // Handle cancellations
                    NSLog(@"Cancelled");
                } else {
                    if ([result.grantedPermissions containsObject:@"public_profile"]) {
                        // Do work
                        FBSDKGraphRequest *request=[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"name,picture", @"locale":@"RU_RU"}];
                        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                            [defaults setValue:[result valueForKey:@"id"] forKey:@"fb_id"];
                            [defaults synchronize];
                            [LFMaster refreshFBStats];
                            [self addSocialWithType:@"fb" andID:[result valueForKey:@"id"]];
//                            [self loginOrRegister];
                        }];
                    }
                }
            }];
        }
    }
    else
    {
        if (self.topCell.fbButton.alpha==1)
        {
//            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"fb://profile?app_scoped_user_id=%@",[defaults valueForKey:@"fb_id"]]]])
//            else
            {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.facebook.com/app_scoped_user_id/%@",[defaults valueForKey:@"fb_id"]]]];
            }
        }
    }
}

- (IBAction)twitterButtonPressed:(id)sender {
    if (self.isEditing)
    {
        if (self.topCell.twitterButton.alpha==1)
        {
            UIAlertController* controller=[UIAlertController alertControllerWithTitle:loc(@"LOGOUT?") message:@"Twitter" preferredStyle:UIAlertControllerStyleAlert];
            [controller addAction:[UIAlertAction actionWithTitle:@"ОK" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                [self removeSocialWithType:@"twitter"];
                [[[Twitter sharedInstance] sessionStore] logOutUserID:[NSString stringWithFormat:@"%@",[defaults valueForKey:@"twitter_true_id"]]];
            }]];
            [controller addAction:[UIAlertAction actionWithTitle:loc(@"CANCEL") style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:controller animated:YES completion:nil];
        }
        else
        {
             [[[Twitter sharedInstance] sessionStore] logOutUserID:@"2409332024"];
            [[Twitter sharedInstance] logInWithMethods:TWTRLoginMethodWebBasedForceLogin completion:^
             (TWTRSession *session, NSError *error) {
                 if (session) {
                     [[[TWTRAPIClient alloc] initWithUserID:[session userID]] loadUserWithID:[session userID] completion:^(TWTRUser * _Nullable user, NSError * _Nullable error) {
                         [defaults setValue:[user screenName] forKey:@"twitter_id"];
                         [defaults setValue:[user userID] forKey:@"twitter_true_id"];
                         [defaults synchronize];
                         
//                         [defaults setValue:@"twitter" forKey:@"type"];
                         [LFMaster refreshTwitterStats];
                         [self addSocialWithType:@"twitter" andID:[user screenName]];
                         
                         
//                         [self loginOrRegister];
//                         [SVProgressHUD show];
                     }];
                 } else {
                     NSLog(@"Error: %@", [error localizedDescription]);
                 }
             }];
        }
    }
    else
    {
        if (self.topCell.twitterButton.alpha==1)
        {
            if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"twitter:///user?screen_name=%@",[defaults valueForKey:@"twitter_id"]]]])
            {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://twitter.com/%@",[defaults valueForKey:@"twitter_id"]]]];
            }
            else
            {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://twitter.com/%@",[defaults valueForKey:@"twitter_id"]]]];
            }
        }
    }
}

-(void)removeSocialWithType:(NSString*)type
{
    [[[LFRequester sharedInstanse] sessionManager] POST:@"removeSocial" parameters:@{@"user_id":[defaults valueForKey:@"myID"],@"type":type} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary* user=[responseObject valueForKey:@"user"];
        [defaults removeObjectForKey:@"vk_id"];
        [defaults removeObjectForKey:@"fb_id"];
        [defaults removeObjectForKey:@"insta_id"];
        [defaults removeObjectForKey:@"twitter_id"];
        if (![[user valueForKey:@"vk"] isKindOfClass:[NSNull class]]) [defaults setValue:[user valueForKey:@"vk"] forKey:@"vk_id"];
        if (![[user valueForKey:@"fb"] isKindOfClass:[NSNull class]]) [defaults setValue:[user valueForKey:@"fb"] forKey:@"fb_id"];
        if (![[user valueForKey:@"insta"] isKindOfClass:[NSNull class]]) [defaults setValue:[user valueForKey:@"insta"] forKey:@"insta_id"];
        if (![[user valueForKey:@"twitter"] isKindOfClass:[NSNull class]]) [defaults setValue:[user valueForKey:@"twitter"] forKey:@"twitter_id"];
        [defaults synchronize];
        [self.tableView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
    }];
}

- (IBAction)addContactButtonPressed:(id)sender {
    UIAlertController* alert=[UIAlertController alertControllerWithTitle:loc(@"ADD_LINK") message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder=loc(@"LINK_TITLE");
        textField.autocapitalizationType=UITextAutocapitalizationTypeSentences;
    }];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder=loc(@"LINK_URL");
    }];
    [alert addAction:[UIAlertAction actionWithTitle:@"ОК" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [SVProgressHUD show];
        [[[LFRequester sharedInstanse] sessionManager] POST:@"createLink" parameters:@{@"user_id":[defaults valueForKey:@"myID"], @"title":alert.textFields[0].text, @"link":alert.textFields[1].text} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            self.links=[responseObject copy];
            [self.tableView reloadData];
            [SVProgressHUD dismiss];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"%@",error);
            [SVProgressHUD showErrorWithStatus:loc(@"ERROR_LINK")];
        }];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:loc(@"CANCEL") style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)instaLoggedIn:(NSDictionary *)dictionary
{
    [self addSocialWithType:@"insta" andID:[[dictionary valueForKey:@"user"] valueForKey:@"username"]];
    
//    [self loginOrRegister];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[InstaViewController class]])
    {
        InstaViewController* controller=segue.destinationViewController;
        controller.delegate=self;
    }
}

-(void)addSocialWithType:(NSString*)type andID:(NSString*)identifier
{
    [[[LFRequester sharedInstanse] sessionManager] POST:@"addSocial" parameters:@{@"type":type,@"social_id":identifier, @"user_id":[defaults valueForKey:@"myID"]} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject valueForKey:@"status"] && [[responseObject valueForKey:@"status"] intValue]==0)
        {
            [SVProgressHUD showErrorWithStatus:loc(@"NETWORK_ALREADY_BENT")];
        }
        else
        {
            NSDictionary* user=[responseObject valueForKey:@"user"];
            if (![[user valueForKey:@"vk"] isKindOfClass:[NSNull class]]) [defaults setValue:[user valueForKey:@"vk"] forKey:@"vk_id"];
            if (![[user valueForKey:@"fb"] isKindOfClass:[NSNull class]]) [defaults setValue:[user valueForKey:@"fb"] forKey:@"fb_id"];
            if (![[user valueForKey:@"insta"] isKindOfClass:[NSNull class]]) [defaults setValue:[user valueForKey:@"insta"] forKey:@"insta_id"];
            if (![[user valueForKey:@"twitter"] isKindOfClass:[NSNull class]]) [defaults setValue:[user valueForKey:@"twitter"] forKey:@"twitter_id"];
            [defaults synchronize];
            [self.tableView reloadData];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
    }];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
