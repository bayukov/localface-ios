//
//  LFMaster.m
//  localface
//
//  Created by Кирилл Баюков on 17.04.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//

#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "LFMaster.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "LFRequester.h"
#import "LFDefines.h"
#import "NSDate+DateTools.h"
#import "VKSdk.h"
#import <TwitterKit/TwitterKit.h>

@implementation LFMaster

+(void)refreshAllStats
{
    if ([defaults valueForKey:@"myID"])
    {
        if ([defaults valueForKey:@"vk_id"])
        {
            [self refreshVKStats];
        }
        if ([defaults valueForKey:@"fb_id"])
        {
            [self refreshFBStats];
        }
        if ([defaults valueForKey:@"twitter_id"])
        {
            [self refreshTwitterStats];
        }
    }
}

+(void)requestShowingMyPosition
{
    if ([defaults valueForKey:@"lastShown"] || [[NSDate date] timeIntervalSince1970]-[[defaults valueForKey:@"lastShown"] intValue]<=7200)
    {
        
    }
    else
    {
        UIAlertController* alert=[UIAlertController alertControllerWithTitle:loc(@"SHOW_YOUR_PLACE?") message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:loc(@"SHOW_PLACE") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[LFRequester sharedInstanse] setNewRadius:[[NSUserDefaults standardUserDefaults] valueForKey:@"radius"]];
            [defaults setValue:@([[NSDate date] timeIntervalSince1970]) forKey:@"lastShown"];
            [defaults synchronize];
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:loc(@"DONT_SHOW_PLACE") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [[LFRequester sharedInstanse] setNewRadius:@0];
        }]];
        UITabBarController* tabbar=[[[UIApplication sharedApplication].windows objectAtIndex:0] rootViewController];
        UINavigationController* nav=[tabbar selectedViewController];
        UIViewController* controller=nav.viewControllers[0];
        [controller presentViewController:alert animated:YES completion:nil];
    }
    
}

+(void)refreshFBStats
{
    if ([defaults valueForKey:@"fb_id"])
    {
        FBSDKGraphRequest *request=[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"music.limit(1000),likes.limit(20),friends.limit(1000),posts.limit(20),video.watches.limit(1000)", @"locale":@"RU_RU"}];
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            NSMutableDictionary* dict=[[NSMutableDictionary alloc] init];
            if ([result valueForKey:@"music"])
            {
                [dict setValue:@([(NSArray*)[[result valueForKey:@"music"] valueForKey:@"data"] count]) forKey:@"fb_music"];
            }
            if ([result valueForKey:@"music"])
            {
                [dict setValue:@([(NSArray*)[[result valueForKey:@"music"] valueForKey:@"data"] count]) forKey:@"fb_music"];
            }
            if ([result valueForKey:@"video.watches"])
            {
                [dict setValue:@([(NSArray*)[[result valueForKey:@"video.watches"] valueForKey:@"data"] count]) forKey:@"fb_movies"];
            }
            if ([result valueForKey:@"friends"])
            {
                [dict setValue:[[[result valueForKey:@"friends"] valueForKey:@"summary"] valueForKey:@"total_count"] forKey:@"fb_friends"];
            }
            if ([result valueForKey:@"posts"])
            {
                NSArray* posts=[[result valueForKey:@"posts"] valueForKey:@"data"];
                NSString* latestDate=[[posts objectAtIndex:[posts count]-1] valueForKey:@"created_time"];
                NSDateFormatter* formatter=[[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
                NSDate* date=[formatter dateFromString:latestDate];
                float daysAgo=[date daysAgo];
                [dict setValue:@((float)[posts count]/daysAgo) forKey:@"fb_posts"];
            }
            if ([result valueForKey:@"likes"])
            {
                NSArray* likes=[[result valueForKey:@"likes"] valueForKey:@"data"];
                NSString* latestDate=[[likes objectAtIndex:[likes count]-1] valueForKey:@"created_time"];
                NSDateFormatter* formatter=[[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
                NSDate* date=[formatter dateFromString:latestDate];
                float daysAgo=[date daysAgo];
                [dict setValue:@((float)[likes count]/daysAgo) forKey:@"fb_likes"];
            }
            [dict setValue:[defaults valueForKey:@"myID"] forKey:@"user_id"];
            [[[LFRequester sharedInstanse] sessionManager] POST:@"refreshStats" parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"STATS_REFRESHED" object:nil];
            } failure:nil];
        }];
    }
}

+(void)refreshVKStats
{
    VKRequest* getMessages=[VKRequest requestWithMethod:@"messages.get" parameters:@{@"count":@200,@"offset":@0,@"out":@1}];
    getMessages.preferredLang=@"ru";
    [getMessages executeWithResultBlock:^(VKResponse *response) {
        NSArray* arr=[response.json valueForKey:@"items"];
        if (arr.count>0)
        {
            NSDictionary* last=[arr objectAtIndex:[arr count]-1];
            NSDate* date=[NSDate dateWithTimeIntervalSince1970:[[last valueForKey:@"date"] intValue]];
            NSInteger hoursAgo=[date hoursAgo];
            if (hoursAgo==0) hoursAgo=1;
            float ratio=(float)[arr count]/hoursAgo*24;
            [defaults setFloat:ratio forKey:@"vkMessageRatio"];
        }
        else
        {
            [defaults setFloat:0 forKey:@"vkMessageRatio"];
        }
        [defaults synchronize];
        
        VKRequest* getFriends=[VKRequest requestWithMethod:@"friends.get" parameters:nil];
        getFriends.preferredLang=@"ru";
        [getFriends executeWithResultBlock:^(VKResponse *response) {
            [defaults setValue:[response.json valueForKey:@"count"] forKey:@"vkFriendsAmount"];
            [defaults synchronize];
            
            VKRequest* getFeed=[VKRequest requestWithMethod:@"newsfeed.get" parameters:@{@"count":@100}];
            getFeed.preferredLang=@"ru";
            [getFeed executeWithResultBlock:^(VKResponse *response) {
                if ([(NSArray*)[response.json valueForKey:@"items"] count]!=0)
                {
                    NSDictionary* last=[[response.json valueForKey:@"items"] objectAtIndex:[(NSArray*)[response.json valueForKey:@"items"] count]-1];
                    NSDate* date=[NSDate dateWithTimeIntervalSince1970:[[last valueForKey:@"date"] longLongValue]];
                    NSInteger hoursAgo=[date hoursAgo];
                    if (hoursAgo==0) hoursAgo=1;
                    float ratio=(float)[(NSArray*)[response.json valueForKey:@"items"] count]/hoursAgo*24;
                    
                    [defaults setFloat:ratio forKey:@"vkFeedRatio"];
                }
                else
                {
                    [defaults setFloat:0 forKey:@"vkFeedRatio"];
                }
                [defaults synchronize];
                
                
                VKRequest* getWall=[VKRequest requestWithMethod:@"wall.get" parameters:@{@"count":@20,@"filter":@"owner",@"offset":@0}];
                getWall.preferredLang=@"ru";
                [getWall executeWithResultBlock:^(VKResponse *response) {
                    if ([(NSArray*)[response.json valueForKey:@"items"] count]!=0)
                    {
                        NSDictionary* last=[[response.json valueForKey:@"items"] objectAtIndex:[(NSArray*)[response.json valueForKey:@"items"] count]-1];
                        NSDate* date=[NSDate dateWithTimeIntervalSince1970:[[last valueForKey:@"date"] longLongValue]];
                        NSInteger daysAgo=[date hoursAgo];
                        if (daysAgo==0) daysAgo=1;
                        float ratio=(float)[(NSArray*)[response.json valueForKey:@"items"] count]/daysAgo*24;
                        [defaults setFloat:ratio forKey:@"vkPostsRatio"];
                    }
                    else
                    {
                        [defaults setFloat:0 forKey:@"vkPostsRatio"];
                    }
                    [defaults synchronize];
                    
                    VKRequest* getMusicAndMovies=[VKRequest requestWithMethod:@"users.get" parameters:@{@"fields":@"counters"}];
                    [getMusicAndMovies executeWithResultBlock:^(VKResponse *response) {
                        [defaults setValue:[[response.json valueForKey:@"counters"] valueForKey:@"audios"][0] forKey:@"vkAudios"];
                        [defaults setValue:[[response.json valueForKey:@"counters"] valueForKey:@"videos"][0] forKey:@"vkVideos"];
                        [defaults synchronize];
                        [self sendStatsFromVK];
                    } errorBlock:^(NSError *error) {
                        NSLog(@"%@",error);
                    }];
                } errorBlock:^(NSError *error) {
                    NSLog(@"%@",error);
                }];
                
                
            } errorBlock:^(NSError *error) {
                NSLog(@"%@",error);
            }];
            
        } errorBlock:^(NSError *error) {
            NSLog(@"%@",error);
        }];
        
    } errorBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
}

+(void)sendStatsFromVK
{
    if ([defaults valueForKey:@"myID"])
    {
        NSMutableDictionary* dict=[[NSMutableDictionary alloc] init];
        [dict setValue:[defaults valueForKey:@"vkMessageRatio"] forKey:@"vk_messages"];
        [dict setValue:[defaults valueForKey:@"vkFeedRatio"] forKey:@"vk_feed"];
        [dict setValue:[defaults valueForKey:@"vkFriendsAmount"] forKey:@"vk_friends"];
        [dict setValue:[defaults valueForKey:@"vkPostsRatio"] forKey:@"vk_posts"];
        [dict setValue:[defaults valueForKey:@"vkAudios"] forKey:@"vk_music"];
        [dict setValue:[defaults valueForKey:@"vkVideos"] forKey:@"vk_movies"];
        [dict setValue:[defaults valueForKey:@"myID"] forKey:@"user_id"];
        [[[LFRequester sharedInstanse] sessionManager] POST:@"refreshStats" parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"STATS_REFRESHED" object:nil];
        } failure:nil];
    }
}


+(void)refreshTwitterStats
{
    [[TWTRAPIClient clientWithCurrentUser] sendTwitterRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://api.twitter.com/1.1/statuses/user_timeline.json?user_id=%@&count=200",[defaults valueForKey:@"twitter_true_id"]]]] completion:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        NSArray* arr=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSPredicate* retweeted=[NSPredicate predicateWithFormat:@"retweeted == 1"];
        NSArray* retweetedArr=[arr filteredArrayUsingPredicate:retweeted];
        NSPredicate* notRetweeted=[NSPredicate predicateWithFormat:@"retweeted == 0"];
        NSArray* notRetweetedArr=[arr filteredArrayUsingPredicate:notRetweeted];
        
        NSDateFormatter *dateFormatter= [NSDateFormatter new];
        dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormatter setDateFormat:@"EEE MMM dd HH:mm:ss Z yyyy"];
        
        
        float ratioRetweeted=0;
        if (retweetedArr.count>0)
        {
            NSDictionary* latestRetweeted=[retweetedArr objectAtIndex:[retweetedArr count]-1];
            NSDate* latestDateRetweeted=[dateFormatter dateFromString:[latestRetweeted valueForKey:@"created_at"]];
            NSInteger daysAgoRetweeted=[latestDateRetweeted daysAgo];
            if (daysAgoRetweeted==0) daysAgoRetweeted=1;
            ratioRetweeted=(float)[retweetedArr count]/daysAgoRetweeted;
        }
        
        float ratioNotRetweeted=0;
        if (notRetweetedArr.count>0)
        {
            NSDictionary* latestNotRetweeted=[notRetweetedArr objectAtIndex:[notRetweetedArr count]-1];
            NSDate* latestDateNotRetweeted=[dateFormatter dateFromString:[latestNotRetweeted valueForKey:@"created_at"]];
            NSInteger daysAgoNotRetweeted=[latestDateNotRetweeted daysAgo];
            if (daysAgoNotRetweeted==0) daysAgoNotRetweeted=1;
            ratioNotRetweeted=(float)[notRetweetedArr count]/daysAgoNotRetweeted;
        }
        NSLog(@"%f %f",ratioRetweeted, ratioNotRetweeted);
        [defaults setFloat:ratioRetweeted forKey:@"tw_retweeted"];
        [defaults setFloat:ratioNotRetweeted forKey:@"tw_posts"];
        [defaults synchronize];
        //социальность
        [[TWTRAPIClient clientWithCurrentUser] sendTwitterRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://api.twitter.com/1.1/users/show.json?user_id=%@",[defaults valueForKey:@"twitter_true_id"]]]] completion:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
            NSDictionary* dict=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSInteger friends=[[dict valueForKey:@"friends_count"] integerValue]+[[dict valueForKey:@"followers_count"] integerValue];
            [defaults setInteger:friends forKey:@"tw_friends"];
            [defaults synchronize];
            [self sendStatsFromTW];
        }];
    }];
    
}

+(void)sendStatsFromTW
{
    if ([defaults valueForKey:@"myID"])
    {
        NSMutableDictionary* dict=[[NSMutableDictionary alloc] init];
        [dict setValue:[defaults valueForKey:@"tw_friends"] forKey:@"tw_friends"];
        [dict setValue:[defaults valueForKey:@"tw_retweeted"] forKey:@"tw_retweets"];
        [dict setValue:[defaults valueForKey:@"tw_posts"] forKey:@"tw_posts"];
        [dict setValue:[defaults valueForKey:@"myID"] forKey:@"user_id"];
        [[[LFRequester sharedInstanse] sessionManager] POST:@"refreshStats" parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"STATS_REFRESHED" object:nil];
        } failure:nil];
    }
}

@end
