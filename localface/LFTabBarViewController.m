//
//  LFTabBarViewController.m
//  localface
//
//  Created by Кирилл Баюков on 22.03.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//
#import "UIColor+myColor.h"
#import "LFTabBarViewController.h"
#import "LFDefines.h"

@interface LFTabBarViewController ()

@end

@implementation LFTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSArray* titles=@[loc(@"PROFILE"), loc(@"MAP"),loc(@"SETTINGS")];
    for (UITabBarItem* item in self.tabBar.items)
    {
        int index=[self.tabBar.items indexOfObject:item];
        item.title=titles[index];
        if (index==0) item.image=[UIImage imageNamed:@"профиль"];
        if (index==1) item.image=[UIImage imageNamed:@"карта"];
        if (index==2) item.image=[UIImage imageNamed:@"settings"];
    }
    self.selectedIndex=1;
    self.tabBar.unselectedItemTintColor=[UIColor colorWithWhite:0.62 alpha:1];
    self.tabBar.tintColor=[UIColor myPinkColor];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didOpenNotification) name:@"DID_OPEN_NOTIFICATION" object:nil];
    // Do any additional setup after loading the view.
}

-(void)didOpenNotification
{
    if ([defaults valueForKey:@"idToOpen"] && (self.selectedIndex==0 || self.selectedIndex==2))
    {
        self.selectedIndex=1;
        [(UINavigationController*)self.selectedViewController popToRootViewControllerAnimated:YES];
    }
    else if ([defaults valueForKey:@"idToOpen"] && self.selectedIndex==1)
    {
        [(UINavigationController*)self.selectedViewController popToRootViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
