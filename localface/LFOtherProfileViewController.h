//
//  LFOtherProfileViewController.h
//  localface
//
//  Created by Кирилл Баюков on 22.03.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LFOtherProfileViewController : UIViewController
@property (nonatomic) int userID;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
