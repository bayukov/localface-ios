//
//  InstaViewController.h
//  dogs
//
//  Created by User on 15.10.15.
//  Copyright © 2015 AppCraft. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol InstaViewControllerDelegate <NSObject>

-(void)instaLoggedIn:(NSDictionary*)dictionary;

@end

@interface InstaViewController : UIViewController

@property (weak, nonatomic) id<InstaViewControllerDelegate> delegate;
- (IBAction)back:(id)sender;

@end
