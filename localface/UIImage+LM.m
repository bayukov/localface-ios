//
//  UIImage+LM.m
//  largessme
//
//  Created by Александр Мурзанаев on 01.10.15.
//  Copyright © 2015 AppCraft. All rights reserved.
//

#import "UIImage+LM.h"

@implementation UIImage (LM)

- (UIColor *)averageColor {
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char rgba[4];
    CGContextRef context = CGBitmapContextCreate(rgba, 1, 1, 8, 4, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGContextDrawImage(context, CGRectMake(0, 0, 1, 1), self.CGImage);
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    
    if (rgba[3] > 0) {
        CGFloat alpha = ((CGFloat)rgba[3])/255.0;
        CGFloat multiplier = alpha/255.0;
        return [UIColor colorWithRed:((CGFloat)rgba[0])*multiplier
                               green:((CGFloat)rgba[1])*multiplier
                                blue:((CGFloat)rgba[2])*multiplier
                               alpha:alpha];
    }
    else {
        return [UIColor colorWithRed:((CGFloat)rgba[0])/255.0
                               green:((CGFloat)rgba[1])/255.0
                                blue:((CGFloat)rgba[2])/255.0
                               alpha:((CGFloat)rgba[3])/255.0];
    }
}

-(UIImage *)shrinkedImage {
    CGSize initialSize = self.size;
    CGSize size;
    const float edge = 750; // iPhone 6 width
    if (initialSize.width > initialSize.height)
        size = CGSizeMake(edge * (initialSize.width / initialSize.height), edge);
    else
        size = CGSizeMake(edge, edge * (initialSize.height / initialSize.width));
    UIGraphicsBeginImageContextWithOptions(size, NO, 1.0);
    [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
