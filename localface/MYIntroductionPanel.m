//
//  MYIntroductionPanel.m
//  MYBlurIntroductionView-Example
//
//  Created by Matthew York on 10/16/13.
//  Copyright (c) 2013 Matthew York. All rights reserved.
//

#import "MYIntroductionPanel.h"
#import "UIColor+myColor.h"
#import "LFDefines.h"

@implementation MYIntroductionPanel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initializeConstants];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame title:(NSString *)title description:(NSString *)description{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initializeConstants];
        
        self.PanelTitle = title;
        self.PanelDescription = description;
        [self buildPanelWithFrame:frame];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame title:(NSString *)title description:(NSString *)description header:(UIView *)headerView{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initializeConstants];
        self.PanelHeaderView = headerView;
        self.PanelTitle = title;
        self.PanelDescription = description;
        [self buildPanelWithFrame:frame];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame title:(NSString *)title description:(NSString *)description image:(UIImage *)image{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initializeConstants];
        
        self.PanelTitle = title;
        self.PanelDescription = description;
        self.PanelImageView = [[UIImageView alloc] initWithImage:image];
        [self buildPanelWithFrame:frame];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame title:(NSString *)title description:(NSString *)description image:(UIImage *)image header:(UIView *)headerView{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initializeConstants];
        
        self.PanelHeaderView = headerView;
        self.PanelTitle = title;
        self.PanelDescription = description;
        self.PanelImageView = [[UIImageView alloc] initWithImage:image];
        [self buildPanelWithFrame:frame];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame nibNamed:(NSString *)nibName{
    self = [super init];
    if (self) {
        // Initialization code
        self = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil][0];
        self.frame = frame;
        self.isCustomPanel = YES;
        self.hasCustomAnimation = NO;
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame title:(NSString *)title description:(NSString *)description image:(UIImage *)image icon:(UIImage*)icon
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initializeConstants];
        self.PanelTitle = title;
        self.PanelDescription = description;
        self.PanelImageView = [[UIImageView alloc] initWithImage:image];
        self.PanelIcon=[[UIImageView alloc] initWithImage:icon];
        [self buildPanelWithFrame:frame];
    }
    return self;
}

-(void)initializeConstants{
    kTitleFont = [UIFont boldSystemFontOfSize:21];
    //kTitleTextColor = [UIColor whiteColor];
    kDescriptionFont = [UIFont systemFontOfSize:16];
    //kDescriptionTextColor = [UIColor whiteColor];
    kSeparatorLineColor = [UIColor colorWithWhite:0 alpha:0.1];
    
    self.backgroundColor = [UIColor whiteColor];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)buildPanelWithFrame:(CGRect)frame{
    CGFloat panelTitleHeight = 0;
    CGFloat panelDescriptionHeight = 0;
    
    CGFloat runningYOffset = kTopPadding + frame.size.height * 0.5;
    CGFloat initialYOffset = runningYOffset;
    
    //Process panel header view, if it exists
    if (self.PanelHeaderView) {
        self.PanelHeaderView.frame = CGRectMake((frame.size.width - self.PanelHeaderView.frame.size.width)/2, runningYOffset, self.PanelHeaderView.frame.size.width, self.PanelHeaderView.frame.size.height);
        [self addSubview:self.PanelHeaderView];
        
        runningYOffset += self.PanelHeaderView.frame.size.height + kHeaderTitlePadding;
    }
    
    //Calculate title and description heights
    if ([MYIntroductionPanel runningiOS7]) {
        //Calculate Title Height
        NSDictionary *titleAttributes = [NSDictionary dictionaryWithObject:kTitleFont forKey: NSFontAttributeName];
        panelTitleHeight = [self.PanelTitle boundingRectWithSize:CGSizeMake(frame.size.width - 2*kLeftRightMargins, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:titleAttributes context:nil].size.height;
        panelTitleHeight = ceilf(panelTitleHeight);
        
        //Calculate Description Height
        NSDictionary *descriptionAttributes = [NSDictionary dictionaryWithObject:kDescriptionFont forKey: NSFontAttributeName];
        panelDescriptionHeight = [self.PanelDescription boundingRectWithSize:CGSizeMake(frame.size.width - 2*kLeftRightMargins, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:descriptionAttributes context:nil].size.height;
        panelDescriptionHeight = ceilf(panelDescriptionHeight);
    }
    else {
        panelTitleHeight = [self.PanelTitle sizeWithFont:kTitleFont constrainedToSize:CGSizeMake(frame.size.width - 2*kLeftRightMargins, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping].height;
        
        panelDescriptionHeight = [self.PanelDescription sizeWithFont:kDescriptionFont constrainedToSize:CGSizeMake(frame.size.width - 2*kLeftRightMargins, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping].height;
    }
    
    //Create title label
//    self.PanelTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kLeftRightMargins, runningYOffset, frame.size.width - 2*kLeftRightMargins, panelTitleHeight)];
//    self.PanelTitleLabel.numberOfLines = 0;
//    self.PanelTitleLabel.text = self.PanelTitle;
//    self.PanelTitleLabel.font = kTitleFont;
//    self.PanelTitleLabel.textColor = kTitleTextColor;
//    self.PanelTitleLabel.alpha = 0;
//    self.PanelTitleLabel.backgroundColor = [UIColor clearColor];
//    [self addSubview:self.PanelTitleLabel];
//    runningYOffset += panelTitleHeight + kTitleDescriptionPadding;
    
    
    //Add small line in between title and description
//    self.PanelSeparatorLine = [[UIView alloc] initWithFrame:CGRectMake(kLeftRightMargins, runningYOffset - 0.5*kTitleDescriptionPadding, frame.size.width - 2*kLeftRightMargins, 1)];
//    self.PanelSeparatorLine.backgroundColor = kSeparatorLineColor;
//     [self addSubview:self.PanelSeparatorLine];
    
    //Create description label
    
    
    runningYOffset += panelDescriptionHeight + kDescriptionImagePadding;
    UIInterpolatingMotionEffect *verticalMotionEffect1 = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    verticalMotionEffect1.minimumRelativeValue = @(-10);
    verticalMotionEffect1.maximumRelativeValue = @(10);
    
    UIInterpolatingMotionEffect *horizontalMotionEffect1 = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalMotionEffect1.minimumRelativeValue = @(-10);
    horizontalMotionEffect1.maximumRelativeValue = @(10);
    
    UIMotionEffectGroup *group1 = [UIMotionEffectGroup new];
    group1.motionEffects = @[horizontalMotionEffect1, verticalMotionEffect1];
    
    UIInterpolatingMotionEffect *verticalMotionEffect2 = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    verticalMotionEffect2.minimumRelativeValue = @(-20);
    verticalMotionEffect2.maximumRelativeValue = @(20);
    
    UIInterpolatingMotionEffect *horizontalMotionEffect2 = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalMotionEffect2.minimumRelativeValue = @(-20);
    horizontalMotionEffect2.maximumRelativeValue = @(20);
    
    UIMotionEffectGroup *group2 = [UIMotionEffectGroup new];
    group2.motionEffects = @[horizontalMotionEffect2, verticalMotionEffect2];
    
    UIInterpolatingMotionEffect *verticalMotionEffect3 = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    verticalMotionEffect3.minimumRelativeValue = @(-30);
    verticalMotionEffect3.maximumRelativeValue = @(30);
    
    UIInterpolatingMotionEffect *horizontalMotionEffect3 = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalMotionEffect3.minimumRelativeValue = @(-30);
    horizontalMotionEffect3.maximumRelativeValue = @(30);
    
    UIMotionEffectGroup *group3 = [UIMotionEffectGroup new];
    group3.motionEffects = @[horizontalMotionEffect3, verticalMotionEffect3];
    //Add image, if there is room
    //if (self.backgroundImage.image) {
        //CGFloat imageSize = frame.size.width * 0.4f;
//        self.PanelImageView.frame = CGRectMake(kLeftRightMargins, runningYOffset, self.frame.size.width - 2*kLeftRightMargins, self.frame.size.height - runningYOffset - kBottomPadding);
    self.backgroundImage=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background_tutor"]];
    self.backgroundImage.center=CGPointMake(self.center.x, self.center.y-60);
    
    self.backgroundImage.contentMode = UIViewContentModeScaleAspectFit;
    self.backgroundImage.clipsToBounds = YES;
    
    [self insertSubview:self.backgroundImage atIndex:0];
    [self.backgroundImage addMotionEffect:group1];
    self.grassImage=[[UIImageView alloc] initWithFrame:CGRectMake(self.backgroundImage.frame.origin.x, self.backgroundImage.frame.origin.y+self.backgroundImage.frame.size.height, self.backgroundImage.frame.size.width, 30)];
    self.grassImage.contentMode = UIViewContentModeScaleAspectFit;
    self.grassImage.clipsToBounds = YES;
    self.grassImage.image=[UIImage imageNamed:@"grass_1"];
    [self.grassImage addMotionEffect:group2];
    [self addSubview:self.grassImage];
    
    self.text = [[UILabel alloc] initWithFrame:CGRectMake(16, [UIScreen mainScreen].bounds.size.height-200, self.frame.size.width-32, 100)];
    if (IS_IPHONE_4_OR_LESS)
    {
        self.text.frame=CGRectMake(16, self.grassImage.frame.origin.y+8, self.frame.size.width-32, 100);
    }
    self.text.numberOfLines = 0;
    self.text.text = self.PanelDescription;
    self.text.font =  [UIFont systemFontOfSize:24];
//    self.text.textColor = [UIColor nameColor];
    self.text.alpha = 1;
    self.text.textAlignment=NSTextAlignmentCenter;
    //self.PanelDescriptionLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:self.text];
    
    if ([self.text.text isEqualToString:@"Бесплатно находите новых людей на карте"])
    {
        self.boy2=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tutor1"]];
        self.boy2.center=CGPointMake(self.backgroundImage.center.x, self.grassImage.frame.origin.y-self.boy2.frame.size.height/2+100);
        [self addSubview:self.boy2];
    }
    else if ([self.text.text isEqualToString:@"localface"])
    {
        self.boy2=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tutor2"]];
        self.boy2.center=CGPointMake(self.backgroundImage.center.x, self.grassImage.frame.origin.y-self.boy2.frame.size.height/2+100);
        [self addSubview:self.boy2];
    }
    else
    {
        self.boy2=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tutor3"]];
        self.boy2.center=CGPointMake(self.backgroundImage.center.x, self.grassImage.frame.origin.y-self.boy2.frame.size.height/2+100);
        [self addSubview:self.boy2];
    }
    //}
    
    
//    UIImageView* logo=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo"]];
//    logo.frame=CGRectMake(0, 25, [UIScreen mainScreen].bounds.size.width, 50);
//    logo.contentMode=UIViewContentModeScaleAspectFit;
//    logo.clipsToBounds=YES;
//    [self addSubview:logo];

    // If this is a custom panel, set the has Custom animation boolean
    if (self.isCustomPanel == YES) {
        self.hasCustomAnimation = YES;
    }
}

+(BOOL)runningiOS7{
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if (currSysVer.floatValue >= 7.0) {
        return YES;
    }
    
    return NO;
}

#pragma mark - Interaction Methods

-(void)panelDidAppear{
    //Implemented by subclass
}

-(void)panelDidDisappear{
    //Implemented by subclass
}


@end
