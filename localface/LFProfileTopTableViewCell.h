//
//  LFProfileTopTableViewCell.h
//  localface
//
//  Created by Кирилл Баюков on 23.03.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LFProfileTopTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@property (weak, nonatomic) IBOutlet UIButton *vkButton;
@property (weak, nonatomic) IBOutlet UIButton *igButton;
@property (weak, nonatomic) IBOutlet UIButton *fbButton;
@property (weak, nonatomic) IBOutlet UIButton *twitterButton;

@end
