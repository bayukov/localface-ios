//
//  LFMaster.h
//  localface
//
//  Created by Кирилл Баюков on 17.04.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface LFMaster : NSObject
+(void)requestShowingMyPosition;
+(void)refreshFBStats;
+(void)refreshVKStats;
+(void)refreshTwitterStats;
+(void)refreshAllStats;
@end

