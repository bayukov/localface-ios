//
//  LFUserStatsTableViewCell.m
//  localface
//
//  Created by Кирилл Баюков on 25.08.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//

#import "LFUserStatsTableViewCell.h"

@implementation LFUserStatsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reinit
{
    if (self.sociabilityFlag)
    {
        self.sociability.textColor=[UIColor colorWithWhite:86.0/255 alpha:1];
    }
    else
    {
        self.sociability.textColor=[UIColor lightGrayColor];
        self.sociabilityProgressBar.progress=0;
    }
    
    if (self.talkativityFlag)
    {
        self.talkativity.textColor=[UIColor colorWithWhite:86.0/255 alpha:1];
    }
    else
    {
        self.talkativity.textColor=[UIColor lightGrayColor];
        self.talkativityProgressBar.progress=0;
    }
    
    if (self.contentGenerationFlag)
    {
        self.contentGeneration.textColor=[UIColor colorWithWhite:86.0/255 alpha:1];
    }
    else
    {
        self.contentGeneration.textColor=[UIColor lightGrayColor];
        self.contentGenerationProgressBar.progress=0;
    }
    
    if (self.responsivenessFlag)
    {
        self.responsiveness.textColor=[UIColor colorWithWhite:86.0/255 alpha:1];
    }
    else
    {
        self.responsiveness.textColor=[UIColor lightGrayColor];
        self.responsivenessProgressBar.progress=0;
    }
    
    if (self.musicInterestFlag)
    {
        self.musicInterest.textColor=[UIColor colorWithWhite:86.0/255 alpha:1];
    }
    else
    {
        self.musicInterest.textColor=[UIColor lightGrayColor];
        self.musicInterestProgressBar.progress=0;
    }
    
    if (self.videoInterestFlag)
    {
        self.videoInterest.textColor=[UIColor colorWithWhite:86.0/255 alpha:1];
    }
    else
    {
        self.videoInterest.textColor=[UIColor lightGrayColor];
        self.videoInterestProgressBar.progress=0;
    }
}

@end
