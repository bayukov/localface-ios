//
//  LFRadiusViewController.m
//  localface
//
//  Created by Кирилл Баюков on 27.03.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//

#import "LFRadiusViewController.h"
#import "UIColor+myColor.h"
#import "LFRequester.h"
#import "LFDefines.h"

@interface LFRadiusViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) NSArray* options;
@property (strong, nonatomic) NSArray* distances;

@end

@implementation LFRadiusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=loc(@"DISPLAY_RADIUS");
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.rowHeight=50;
    self.options=@[[NSString stringWithFormat:@"10 %@",loc(@"M")], [NSString stringWithFormat:@"50 %@",loc(@"M")],[NSString stringWithFormat:@"100 %@",loc(@"M")],[NSString stringWithFormat:@"200 %@",loc(@"M")],[NSString stringWithFormat:@"500 %@",loc(@"M")],[NSString stringWithFormat:@"1 %@",loc(@"KM")],[NSString stringWithFormat:@"2 %@",loc(@"KM")],[NSString stringWithFormat:@"5 %@",loc(@"KM")],[NSString stringWithFormat:@"10 %@",loc(@"KM")],[NSString stringWithFormat:@"20 %@",loc(@"KM")], loc(@"UNLIMITED"), loc(@"DONT_SHOW")];
    self.distances=@[@10,@50,@100,@200,@500,@1000,@2000,@5000,@10000,@20000,@(-1),@0];
    
    UIBarButtonItem* back=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    back.tintColor=[UIColor myBlackColor];
    self.navigationItem.leftBarButtonItem=back;
    self.tableView.contentInset=UIEdgeInsetsMake(-64, 0, -50, 0);
    self.tableView.scrollIndicatorInsets=UIEdgeInsetsMake(-64, 0, -50, 0);
}

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.options.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    UILabel* optionLabel=[cell viewWithTag:1];
    optionLabel.text=[self.options objectAtIndex:indexPath.row];
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"radius"] intValue]==[self.distances[indexPath.row] intValue])
    {
        cell.backgroundColor=[UIColor colorWithWhite:235.0/255 alpha:1];
    }
    else
    {
        cell.backgroundColor=[UIColor whiteColor];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSUserDefaults standardUserDefaults] setValue:self.distances[indexPath.row] forKey:@"radius"];
    [[LFRequester sharedInstanse] setNewRadius:self.distances[indexPath.row]];
    [self back];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
