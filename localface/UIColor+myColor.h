//
//  UIColor+myColor.h
//  localface
//
//  Created by Кирилл Баюков on 22.03.17.
//  Copyright © 2017 LocalFace. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (myColor)
+(UIColor*)myGrayColor;
+(UIColor*)myPinkColor;
+(UIColor*)myBlackColor;

@end
