//
//  PLDefines.h
//  Plans
//
//  Created by User on 30.08.16.
//  Copyright © 2016 AppCraft. All rights reserved.
//

#ifndef LFDefines_h
#define LFDefines_h

#define defaults [NSUserDefaults standardUserDefaults]
#define UPDATE_PROFILE_IMAGE @"update profile image"
#define ANKETA_SELECTION_CHANGED @"anketa selection changed"
#define SHALL_PRESENT_IMAGEVIEWER @"shall present imageViewer"
#define LOGIN_SUCCESSFUL_REGISTER @"login successful register"
#define LOGIN_NEED_CONFIRMATION @"login need confrimation"
#define LOGIN_SUCCESSFUL_LOGIN @"login successful login"
#define LOGIN_ERROR @"login error"
#define COORDS_UPDATED @"coords updated"
#define COORDS_RECIEVED @"coords recieved"

#define SLIDER_BEGIN_SLIDING @"slider begin sliding"
#define SLIDER_END_SLIDING @"slider end sliding"


#define ADD_PHOTO_PRESSED @"add photo pressed"
#define ADD_VIDEO_PRESSED @"add video pressed"
#define ADD_ALBUM_PRESSED @"add album pressed"

#define OPEN_ALBUM @"open album"

#define OPEN_NOTE @"open note"

#define OPEN_ANKETA @"open anketa"

#define SHALL_RELOAD_FRIENDS @"shall reload friends"

#define NOTE_DELETED @"note deleted"

#define COMMENT_DELETED @"comment deleted"

#define GOT_VIEWS @"got views"

#define OPEN_ANKETA_FROM_NOTE @"open anketa from note"
#define PRESENT_MEDIA_FROM_NOTE @"present media from note"
#define SHOW_TEST_FROM_NOTE @"show test from note"

#define SENT_MESSAGE @"sent message"

#define DELETE_ROW_FROM_MESSAGES @"delete row from messages"
#define DELETED_CATEGORY_MESSAGES @"deleted category messages"


#define ASSOCIATION_PICKED @"association_picked"

#define kInAppPurchaseManagerProductsFetchedNotification @"kInAppPurchaseManagerProductsFetchedNotification"
#define kInAppPurchaseManagerTransactionFailedNotification @"kInAppPurchaseManagerTransactionFailedNotification"
#define kInAppPurchaseManagerTransactionSucceededNotification @"kInAppPurchaseManagerTransactionSucceededNotification"

#define ASS_BOUGHT @"ass bought"
#define ASS_NOT_BOUGHT @"ass not bought"


#define GOT_NEW_MESSAGE @"got new message"

#define ADD_FRIEND @"add friend"


#define loc(str) NSLocalizedString(str, nil)


#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#endif /* PLDefines_h */
