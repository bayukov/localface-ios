//
//  InstaViewController.m
//  dogs
//
//  Created by User on 15.10.15.
//  Copyright © 2015 AppCraft. All rights reserved.
//

#import "InstaViewController.h"
#import "UIColor+myColor.h"
#import "AFNetworking.h"
#import "LFDefines.h"
//#import "InstagramKit.h"
//#import "InstagramEngine.h"


static NSString* const clientId=@"6b529910e8f6491694b6a69919b787c7";   //@"a125e47e88e6480ba9efc0d18f3568d2";
static NSString* const redirectUrl=@"http://appcraft.pro";
static NSString* const clientSecret=@"6d76d638f3e349d2a6822a1eb6f0d158";    //@"88e85a4446a8412da89522ec7c69b6ec";

@interface InstaViewController () <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSMutableData *receivedData;

@end

@implementation InstaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.webView.delegate=self;
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://instagram.com/accounts/logout/"]]];
    
    UIBarButtonItem* back=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStyleDone target:self action:@selector(back)];
    back.tintColor=[UIColor myGrayColor];
    self.navigationItem.leftBarButtonItem=back;
    self.title=loc(@"INSTA_LOGIN");
    // [indicator startAnimating];
    // Do any additional setup after loading the view.
}

-(void)back
{
    
    if (self.navigationController && self.navigationController.viewControllers.count>1)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}


- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    //    [indicator startAnimating];
    if ([[[request URL] host] isEqualToString:@"appcraft.pro"]) {
        NSString* verifier = nil;
        NSArray* urlParams = [[[request URL] query] componentsSeparatedByString:@"&"];
        for (NSString* param in urlParams) {
            NSArray* keyValue = [param componentsSeparatedByString:@"="];
            NSString* key = [keyValue objectAtIndex:0];
            if ([key isEqualToString:@"code"]) {
                verifier = [keyValue objectAtIndex:1];
                break;
            }
        }
        if (verifier) {
            
            NSString *data = [NSString stringWithFormat:@"client_id=%@&client_secret=%@&grant_type=authorization_code&redirect_uri=%@&code=%@",clientId,clientSecret,redirectUrl,verifier];
            
            NSString *url = [NSString stringWithFormat:@"https://api.instagram.com/oauth/access_token"];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:[data dataUsingEncoding:NSUTF8StringEncoding]];
            NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:request delegate:self];
            self.receivedData = [[NSMutableData alloc] init];
        } else {
            // ERROR!
        }
    }
    return YES;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    if ([[NSString stringWithFormat:@"%@",webView.request.URL] isEqualToString:@"https://www.instagram.com/"])
    {
        NSString *url = [NSString stringWithFormat:@"https://api.instagram.com/oauth/authorize/?client_id=%@&redirect_uri=%@&response_type=code&scope=follower_list+basic",clientId,redirectUrl];
        
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data

{
    if ([connection.currentRequest.URL.lastPathComponent isEqualToString:@"access_token"])
    {
        [self.receivedData appendData:data];
        NSError* err;
        id algas = [NSJSONSerialization JSONObjectWithData:self.receivedData options:0 error:&err];
        NSString* token=[algas valueForKey:@"access_token"];
        NSString* apiURL = @"https://api.instagram.com/v1/users/self";
        NSAssert(apiURL, @"Please specify API URL key in your Info.plist");
        AFHTTPSessionManager* manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:apiURL]];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        manager.securityPolicy.allowInvalidCertificates = YES;
        manager.securityPolicy.validatesDomainName = NO;
        [manager GET:[NSString stringWithFormat:@"media/recent/?access_token=%@",token] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"%@",responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"%@",error);
        }];
        if (err) {
            NSLog(@"%@",err);
        } else {
            if (self.delegate) {
                            [self.delegate instaLoggedIn:algas];
            }
            [self back];
        }
    }
    else
    {
        id algas = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"%@", algas);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
