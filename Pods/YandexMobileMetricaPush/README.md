# Yandex AppMetrica Push SDK

## License
License agreement on use of Yandex AppMetrica Push SDK is available at [EULA site] [LICENSE].

## Documentation
Documentation could be found at [Yandex Technologies official site] [DOCUMENTATION].

## Sample
Sample project to use is available at [GitHub] [GitHubSAMPLE].

[LICENSE]: https://yandex.com/legal/metrica_termsofuse/ "Yandex AppMetrica agreement"
[DOCUMENTATION]: https://tech.yandex.com/ "Yandex technologies documentation"
[GitHubSAMPLE]: https://github.com/yandexmobile/metrica-push-sample-ios/ "Yandex AppMetrica Push sample application repository"
