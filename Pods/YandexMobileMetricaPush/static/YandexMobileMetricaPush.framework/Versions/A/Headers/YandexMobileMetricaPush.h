//
//  YandexMobileMetricaPush.h
//
//  This file is a part of the AppMetrica
//
//  Version for iOS © 2016 YANDEX
//
//  You may not use this file except in compliance with the License.
//  You may obtain a copy of the License at http://legal.yandex.com/metrica_termsofuse/
//

#if __has_include("YMPYandexMetricaPush.h")
    #import "YMPYandexMetricaPush.h"
    #import "YMPVersion.h"
#else
    #import <YandexMobileMetricaPush/YMPYandexMetricaPush.h>
    #import <YandexMobileMetricaPush/YMPVersion.h>
#endif

